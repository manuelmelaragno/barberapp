import React from 'react';
import { Dimensions, StyleSheet, Text, View, StatusBar, Platform, AsyncStorage, SafeAreaView, Image, Linking, ScrollView, TouchableWithoutFeedback } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons } from '@expo/vector-icons';
import firebaseAppInitialized from '../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';
import LoadModal from '../loadModal/LoadModal'

export default class Benvenuto extends React.Component {

    constructor() {
        super();

        this.state = {
            loadModalVisibile: false, //Default : false
        }
    }

    async componentDidMount() {
        this.setState({
            loadModalVisibile: true
        })
        // CONTROLLO CHE L'UTENTE ABBIA FATTO L'ACCESSO CON FACEBOOK O SIA RIMASTO CONNESSO
        var rimaniConnesso = '';
        var facebookToken = '';
        var admin = '';
        var user = firebaseAppInitialized.auth().currentUser;
        user === null ? console.log('User non loggato') : console.log('User loggato');

        // VERIFICO CHE L'UTENTE VOGLIA RIMANERE CONNESSO
        await AsyncStorage.getItem('rimaniConnesso', (error, result) => {
            rimaniConnesso = result;
            console.log('Rimani connesso : ' + rimaniConnesso);
        })

        // VERIFICO CHE L'UTENTE SIA UN ADMIN
        await AsyncStorage.getItem('admin', (error, result) => {
            admin = result;
            console.log('Rimani connesso : ' + rimaniConnesso);
        })

        // VERIFICO CHE L'UTENTE ABBIA EFFETTUATO L'ACCESSO CON FB
        await AsyncStorage.getItem('facebookToken', (error, result) => {
            facebookToken = result;
            console.log('facebookToken : ' + facebookToken);
        })

        if (user || (rimaniConnesso !== null && rimaniConnesso !== 'false') || (facebookToken !== null && facebookToken !== '')) {
            //L'UTENTE HA EFFETTUATO L'ACCESSO
            if (user === null && (rimaniConnesso !== null && rimaniConnesso !== 'false')) {
                // ACCEDERE A FIREBASE, L'UTENTE HA IL 'rimaniConnesso'
                var email = '';
                var password = '';

                // PRENDO DALLA MEMORIA L'EMAIL E LA PASSWORD
                await AsyncStorage.getItem('email', (error, result) => {
                    email = result;
                    console.log('Email : ' + email);
                }).then(async () => {
                    await AsyncStorage.getItem('password', (error, result) => {
                        password = result;
                        console.log('Password : ' + password);
                    })
                })

                // ACCEDO 
                firebaseAppInitialized.auth().signInWithEmailAndPassword(email, password)
                    .then(() => {
                        firebaseAppInitialized.auth().app.firestore().collection('Utenti')
                            .doc(email).get()
                            .then(async (doc) => {
                                console.log(doc.data());

                                if (doc.data().admin === true) {
                                    console.log('L\' utente è un admin');
                                    // L'UTENTE E' UN ADMIN
                                    await AsyncStorage.setItem('admin', 'true', (error) => {

                                        error === null ? console.log('Info salvate') : console.log(error);

                                    }).then(() => {
                                        console.log('Accesso effettuato come Admin');
                                        this.refs.toast.show(' Accesso come ADMIN ', 750);
                                        this.setState({
                                            loadModalVisibile: false
                                        })
                                    })
                                } else {
                                    // L'UTENTE NON E' UN ADMIN
                                    await AsyncStorage.setItem('admin', 'false', (error) => {

                                        error === null ? console.log('Non rimane connesso.') : console.log(error);

                                    }).then(() => {
                                        console.log('Accesso effettuato');
                                        this.refs.toast.show('  Accesso effettuato  ', 750);
                                        this.setState({
                                            loadModalVisibile: false
                                        })
                                    })
                                }
                            })
                    })
                    .catch((error) => {
                        console.log('Errors : ' + error);
                        this.refs.toast.show('  Accesso non riuscito, riprovare  ', 750);
                    });
            } else if (user) {
                //L'UTENTE E' GIA' LOGGATO, VERIFICO CHE SIA UN ADMIN
                firebaseAppInitialized.auth().app.firestore().collection('Utenti')
                    .doc(user.email).get()
                    .then(async (doc) => {
                        console.log(doc.data());

                        if (doc.data().admin === true) {
                            console.log('L\' utente è un admin');
                            // L'UTENTE E' UN ADMIN
                            await AsyncStorage.setItem('admin', 'true', (error) => {

                                error === null ? console.log('Info salvate') : console.log(error);

                            }).then(() => {
                                console.log('Accesso effettuato come Admin');
                                this.refs.toast.show(' Accesso come ADMIN ', 750);
                                this.setState({
                                    loadModalVisibile: false
                                })
                            })
                        } else {
                            // L'UTENTE NON E' UN ADMIN
                            await AsyncStorage.setItem('admin', 'false', (error) => {

                                error === null ? console.log('Non rimane connesso.') : console.log(error);

                            }).then(() => {
                                console.log('Accesso effettuato');
                                this.refs.toast.show('  Accesso effettuato  ', 750);
                                this.setState({
                                    loadModalVisibile: false
                                })
                            })
                        }
                    })
            }

        } else {
            console.log('Accesso non effettuato');
            this.refs.toast.show('  Accesso non effettuato  ', 750);
            this.setState({
                loadModalVisibile: false
            })
        }

    }

    render() {
        var { height, width } = Dimensions.get('window');

        const scheme = Platform.select({ ios: 'maps:0,0?q=', android: 'geo:0,0?q=' });
        const latLng = `${41.515745},${14.121348}`;
        const label = 'Centro Commerciale I Melograni';
        const url = Platform.select({
            ios: `${scheme}${label}@${latLng}`,
            android: `${scheme}${latLng}(${label})`
        });


        return (
            <View
                style={{
                    height: "90%",
                    borderWidth: 0,
                }}
            >
                <LoadModal visible={this.state.loadModalVisibile} titolo={'Accedo...'} />
                <ScrollView
                    style={{
                        height: "90%",
                        paddingBottom: 500,
                    }}
                    containerStyle={{
                        flexGrow: 1,
                    }}
                    horizontal={false}
                    scrollEnabled={true}
                    behaviour="height"
                    bounces={false}
                >
                    <Image
                        source={require('../../res/logo.jpg')}
                        style={{ width: width, height: height * 0.4, marginTop: height * 0.015 }}
                        resizeMode='contain'
                    />

                    <Text
                        style={{
                            marginLeft: width * 0.05,
                            marginRight: width * 0.05,
                            fontWeight: '100',
                            fontSize: height * 0.02,
                            textAlignVertical: 'center'
                        }}
                    >
                        {"Benvenuto nell'app "}
                        <Text
                            style={{
                                fontWeight: 'bold',
                                fontSize: height * 0.02
                            }}
                        >
                            {"Marco DiCri’s BarberShop! \n\n"}
                        </Text>
                        <EvilIcons
                            name="location"
                            size={height * 0.04}
                        />
                        {"\nCi puoi trovare al seguente indirizzo : \n"}
                        <Text
                            style={{
                                fontWeight: 'bold',
                                fontSize: height * 0.02,
                                textDecorationLine: 'underline'
                            }}
                            onPress={() => { Linking.openURL(url) }}
                        >
                            {"Roccaravindola (IS), cc “I melograni”, s.s. 85 km28, 100\n\n"}
                        </Text>
                        <EvilIcons
                            name="like"
                            size={height * 0.04}
                        />
                        {"\nPuoi anche trovarci su "}
                        <Text
                            style={{
                                fontWeight: 'bold',
                                fontSize: height * 0.02,
                                textDecorationLine: 'underline'
                            }}
                            onPress={() => { Linking.openURL("https://www.instagram.com/marcodicri_barbershop") }}
                        >
                            {"Instagram"}
                        </Text>
                        {"!\n\n"}
                        <AntDesign
                            name='phone'
                            size={height * 0.03}
                        />
                        {"\nChiamaci al : "}
                        <Text
                            style={{
                                fontWeight: 'bold',
                                fontSize: height * 0.02,
                                textDecorationLine: 'underline'
                            }}
                            onPress={() => {
                                Linking.canOpenURL('tel:+393493920765')
                                    .then(supported => {
                                        if (supported) {
                                            Platform.OS === 'android' ? Linking.openURL('tel:+393493920765') : Linking.openURL('tel://+393493920765').then(promise => { }).catch(error => { });

                                        }
                                    }).catch(error => {
                                        console.log('Errori');

                                    })
                            }}
                        >
                            {"+39 3493920765"}
                        </Text>
                    </Text>
                </ScrollView >
                <Toast
                    ref="toast"
                    fadeOutDuration={250}
                    fadeInDuration={250}
                    textStyle={{
                        fontWeight: '100',
                        color: 'white',
                        fontSize: width * 0.04,
                    }}
                    style={{
                        borderRadius: 100,
                    }}
                    positionValue={height * 0.25}
                />
            </View >
        );
    }
}
