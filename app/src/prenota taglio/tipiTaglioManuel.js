const tipiTaglioManuel = [{
    nome: "Taglio + Shampoo",
    durata: 45,
    costo: "15€",
    id: '0'
},
{
    nome: "Taglio + Shampoo + Barba",
    durata: 60,
    costo: "22€",
    id: '1'
},
{
    nome: "Barba + Trattamento",
    durata: 30,
    costo: "10€",
    id: '2'
},
{
    nome: "Taglio Bimbo fino a 6 anni",
    durata: 25,
    costo: "12€",
    id: '3'
}
]

export default tipiTaglioManuel;