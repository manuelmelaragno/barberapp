import React from 'react';
import { Dimensions, Text, View, TouchableWithoutFeedback, Modal, FlatList } from 'react-native';
import { EvilIcons } from '@expo/vector-icons';

export default class ModalTipoTaglio extends React.Component {

    constructor() {
        super();
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.props.modelSelezioneTipoTaglioVisibility}
                    onRequestClose={() => {
                        this.props.closeModal();
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: 'rgba(0,0,0,0.5)'
                        }}
                    >
                        <View
                            style={{
                                backgroundColor: 'white',
                                width: '95%',
                                height: '60%',
                                alignSelf: 'center',
                                marginTop: 'auto',
                                marginBottom: 'auto',
                                padding: '2%',
                                elevation: 10,

                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 5,
                                },
                                shadowOpacity: 0.34,
                                shadowRadius: 6.27,

                                padding: '5%',

                                borderRadius: 5
                            }}
                        >

                            <View
                                style={{
                                    flexDirection: 'row'
                                }}
                            >
                                <Text
                                    style={{
                                        fontSize: height * 0.025,
                                        fontWeight: 'bold',
                                    }}
                                >
                                    {"Seleziona il tipo di taglio"}
                                </Text>

                                <TouchableWithoutFeedback
                                    onPress={() => {
                                        this.props.closeModal();
                                    }}
                                >
                                    <EvilIcons
                                        name='close'
                                        size={height * 0.05}
                                        style={{
                                            marginRight: 0,
                                            marginLeft: 'auto',
                                        }}
                                    />
                                </TouchableWithoutFeedback>

                            </View>

                            <View
                                style={{
                                    flex: 1,
                                    justifyContent: 'center',
                                    paddingTop: height * 0.1
                                }}
                            >
                                <FlatList

                                    data={this.props.tipiTaglio}
                                    renderItem={({ item }) => <Item titolo={item.nome} costo={item.costo} durata={item.durata} selectTaglio={this.props.selectTaglio} />}
                                    keyExtractor={item => item.id}

                                />
                            </View>

                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

function Item({ titolo, costo, durata, selectTaglio }) {
    var { height, width } = Dimensions.get('window');
    return (
        <TouchableWithoutFeedback
            onPress={() => {
                selectTaglio(titolo, durata);
            }}
        >
            <View
                style={{
                    width: '100%',
                    height: height * 0.075,
                    alignItems: 'center',
                    borderBottomWidth: 0.75,
                    borderBottomRadius: 1,
                    borderBottomStyle: 'dotted',
                    flexDirection: 'row'
                }}
            >
                <View>
                    <Text
                        style={{
                            fontSize: height * 0.02,
                            fontWeight: 'bold',
                            marginLeft: 0
                        }}
                    >
                        {titolo}
                    </Text>
                    <Text
                        style={{
                            fontSize: height * 0.0175,
                            marginRight: 'auto',
                            marginLeft: 0
                        }}
                    >
                        {durata + " Minuti"}
                    </Text>
                </View>

                <Text
                    style={{
                        fontSize: height * 0.02,
                        marginRight: 0,
                        fontWeight: 'bold',
                        marginLeft: 'auto'
                    }}
                >
                    {costo}
                </Text>

            </View>
        </TouchableWithoutFeedback>
    );
}
