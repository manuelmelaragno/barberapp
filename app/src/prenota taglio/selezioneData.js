import React from 'react';
import { Dimensions, StyleSheet, Text, View, StatusBar, Platform, SafeAreaView, Image, Linking, ScrollView, TouchableWithoutFeedback, Modal, TouchableOpacity, FlatList, TouchableNativeFeedback } from 'react-native';
import { Button } from 'react-native-elements';
import { EvilIcons } from '@expo/vector-icons'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { Calendar, CalendarList, Agenda, LocaleConfig } from 'react-native-calendars';
import Constants from 'expo-constants';
import firebaseAppInitialized from '../firebase/firebase'
import tipiTaglioManuel from './tipiTaglioManuel';
import moment from 'moment';

export default class SelezioneData extends React.Component {

    constructor() {
        super();

        this.state = {
            modalSelezioneDataVisibility: false,
            viewSelezioneDataVisibility: true,
            selectedDateString: '',
            selectedDay: '',
            selectedMonth: '',
            selectedYear: '',
            viewSelezioneOrarioVisibility: false,
            prenotazioni: [],
            orariDisponibili: [],
            updateFlatList: true,
            selectedOrario: '',
            markedDates: this.getDaysInMonth(moment().month(), moment().year())
        }

        LocaleConfig.locales['it'] = {
            monthNames: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
            monthNamesShort: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Déc'],
            dayNames: ['Domenica', 'Lunedì', 'Martedì', 'Mercoledì', 'Giovedì', 'Venerdì', 'Sabato'],
            dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
            today: 'Oggi'
        };
        LocaleConfig.defaultLocale = 'it';
    }

    getPrenotazioni = () => {
        console.log('GET PRENOTAZIONI');

        firebaseAppInitialized.auth().app.firestore().collection('Prenotazioni').where("data_prenotazione", "==", this.state.selectedDateString)
            .get()
            .then((querySnapshot) => {
                console.log('QUERY ESEGUITA');

                querySnapshot.forEach(doc => {
                    console.log(doc.data());
                    this.setState({
                        prenotazioni: [...this.state.prenotazioni, doc.data()]
                    }, () => {
                        //console.log(this.state.prenotazioni);
                    })
                })
                this.loadOrari();
            }).catch(error => {
                console.log(error);

            })
    }

    getDaysInMonth(month, year) {
        let pivot = moment().month(month).year(year).startOf('month')
        const end = moment().month(month).year(year).endOf('month')
        const DISABLED_DAYS = ['Sabato', 'Domenica']

        let dates = {}
        const disabled = { disabled: true }
        while (pivot.isBefore(end)) {
            DISABLED_DAYS.forEach((day) => {
                dates[pivot.day(day).format("YYYY-MM-DD")] = disabled
            })
            pivot.add(7, 'days')
        }

        return dates
    }

    loadOrari = () => {
        var orarioApertura = '9:00';
        var orarioChiusura = '19:00';
        var durataTaglio = this.props.durataTaglio;
        console.log(durataTaglio);

        //var durataTaglio = 20; // TEST
        //var tipoTaglio = this.props.taglioSelezionato;
        var tipoTaglio = 'Taglio + Shampoo'; // TEST

        var dataApertura = new Date(this.state.selectedYear, this.state.selectedMonth, this.state.selectedDay, orarioApertura.split(':')[0], orarioApertura.split(':')[1]);
        var dataChiusura = new Date(this.state.selectedYear, this.state.selectedMonth, this.state.selectedDay, orarioChiusura.split(':')[0], orarioChiusura.split(':')[1]);

        var curPrenotazione = 0;

        var dataInizioTaglio = new Date(dataApertura);
        var dataFineTaglio = new Date(dataInizioTaglio);

        var dataCurPrenotazioneInizio = new Date(2000, 1, 1);
        var dataCurPrenotazioneFine = new Date(2000, 1, 1);

        dataFineTaglio.setMinutes(dataFineTaglio.getMinutes() + durataTaglio)

        console.log(dataInizioTaglio);
        console.log(dataFineTaglio);

        if (this.state.prenotazioni !== undefined || this.state.prenotazioni.length > 0) {
            //Ordino le prenotazioni in ordine crescente
            var _prenotazioni = this.state.prenotazioni
                .sort((a, b) => a.orario_inizio.localeCompare(b.orario_inizio));
            console.log(_prenotazioni);

            while (dataFineTaglio.getTime() < dataChiusura.getTime()) {
                console.log('INIZIO FOR');
                dataFineTaglio = new Date(dataInizioTaglio);
                dataFineTaglio.setMinutes(dataFineTaglio.getMinutes() + durataTaglio)

                if (_prenotazioni[curPrenotazione] !== undefined) {
                    dataCurPrenotazioneInizio = new Date(_prenotazioni[curPrenotazione].data_prenotazione.split('-')[0],
                        _prenotazioni[curPrenotazione].data_prenotazione.split('-')[1],
                        _prenotazioni[curPrenotazione].data_prenotazione.split('-')[2],
                        _prenotazioni[curPrenotazione].orario_inizio.split(':')[0],
                        _prenotazioni[curPrenotazione].orario_inizio.split(':')[1]);
                    dataCurPrenotazioneFine = new Date(_prenotazioni[curPrenotazione].data_prenotazione.split('-')[0],
                        _prenotazioni[curPrenotazione].data_prenotazione.split('-')[1],
                        _prenotazioni[curPrenotazione].data_prenotazione.split('-')[2],
                        _prenotazioni[curPrenotazione].orario_fine.split(':')[0],
                        _prenotazioni[curPrenotazione].orario_fine.split(':')[1]);
                    if (dataInizioTaglio.getTime() >= dataCurPrenotazioneInizio.getTime()) { //Esiste una prenotazione a quest'orario, non lo rendo disponibile
                        curPrenotazione++;
                        console.log(dataInizioTaglio.getTime() + '>=' + dataCurPrenotazioneInizio.getTime());
                        dataInizioTaglio = new Date(dataCurPrenotazioneFine);
                    } else {
                        if (dataFineTaglio.getTime() > dataCurPrenotazioneInizio.getTime()) {
                            if (dataFineTaglio.getTime() <= dataCurPrenotazioneFine.getTime()) {
                                console.log(dataFineTaglio.getTime() + '<=' + dataCurPrenotazioneFine.getTime());

                                curPrenotazione++;
                                dataInizioTaglio = new Date(dataCurPrenotazioneFine);
                            } else if (dataFineTaglio.getTime() > dataCurPrenotazioneFine.getTime()) {
                                console.log('dataFineTaglio.getTime() > dataCurPrenotazioneFine.getTime()');
                                curPrenotazione++;
                                dataInizioTaglio = new Date(dataCurPrenotazioneFine);
                            }
                        } else { //Aggiungo agli orari disonibili

                            /*console.log("ORARIO DISPONIBILE : " + dataInizioTaglio.getHours().toString() + ':' + dataInizioTaglio.getMinutes().toString()
                                + " - " + dataFineTaglio.getHours().toString() + ':' + dataFineTaglio.getMinutes().toString());*/
                            /*this.setState({
                                orariDisponibili: [...this.state.orariDisponibili, {
                                    inizio: dataInizioTaglio.getHours().toString() + ':' + dataInizioTaglio.getMinutes().toString(),
                                    fine: dataFineTaglio.getHours().toString() + ':' + dataFineTaglio.getMinutes().toString(),
                                }]
                            })*/
                            var data = {
                                inizio: (dataInizioTaglio.getHours() < 10 ? '0' : '') + dataInizioTaglio.getHours().toString() + ':' + (dataInizioTaglio.getMinutes() < 10 ? '0' : '') + dataInizioTaglio.getMinutes().toString(),
                                fine: (dataFineTaglio.getHours() < 10 ? '0' : '') + dataFineTaglio.getHours().toString() + ':' + (dataFineTaglio.getMinutes() < 10 ? '0' : '') + dataFineTaglio.getMinutes().toString()
                            }

                            this.state.orariDisponibili.push(data);

                            dataInizioTaglio.setMinutes(dataInizioTaglio.getMinutes() + durataTaglio);

                        }
                    }
                } else {
                    /*console.log("ORARIO DISPONIBILE : " + dataInizioTaglio.getHours().toString() + ':' + dataInizioTaglio.getMinutes().toString()
                        + " - " + dataFineTaglio.getHours().toString() + ':' + dataFineTaglio.getMinutes().toString());
                    this.setState({
                        orariDisponibili: [...this.state.orariDisponibili, {
                            inizio: dataInizioTaglio.getHours().toString() + ':' + dataInizioTaglio.getMinutes().toString(),
                            fine: dataFineTaglio.getHours().toString() + ':' + dataFineTaglio.getMinutes().toString(),
                        }]
                    })*/

                    var data = {
                        inizio: (dataInizioTaglio.getHours() < 10 ? '0' : '') + dataInizioTaglio.getHours().toString() + ':' + (dataInizioTaglio.getMinutes() < 10 ? '0' : '') + dataInizioTaglio.getMinutes().toString(),
                        fine: (dataFineTaglio.getHours() < 10 ? '0' : '') + dataFineTaglio.getHours().toString() + ':' + (dataFineTaglio.getMinutes() < 10 ? '0' : '') + dataFineTaglio.getMinutes().toString()
                    }

                    this.state.orariDisponibili.push(data);
                    dataInizioTaglio.setMinutes(dataInizioTaglio.getMinutes() + durataTaglio);
                }
            }

            console.log('FINE FOR');
        } else {

            while (dataFineTaglio.getTime() < dataChiusura.getTime()) {
                var dataFineTaglio = new Date(dataInizioTaglio);
                dataFineTaglio.setMinutes(dataFineTaglio.getMinutes() + durataTaglio)
                /*console.log("ORARIO DISPONIBILE : " + dataInizioTaglio.getHours().toString() + ':' + dataInizioTaglio.getMinutes().toString()
                    + " - " + dataFineTaglio.getHours().toString() + ':' + dataFineTaglio.getMinutes().toString());*/

                var data = {
                    inizio: (dataInizioTaglio.getHours() < 10 ? '0' : '') + dataInizioTaglio.getHours().toString() + ':' + (dataInizioTaglio.getMinutes() < 10 ? '0' : '') + dataInizioTaglio.getMinutes().toString(),
                    fine: (dataFineTaglio.getHours() < 10 ? '0' : '') + dataFineTaglio.getHours().toString() + ':' + (dataFineTaglio.getMinutes() < 10 ? '0' : '') + dataFineTaglio.getMinutes().toString()
                }

                this.state.orariDisponibili.push(data);

                /*this.setState({
                    orariDisponibili: [...this.state.orariDisponibili, data]
                })*/
                dataInizioTaglio.setMinutes(dataInizioTaglio.getMinutes() + durataTaglio);
            }
        }

        console.log(this.state.orariDisponibili);
        this.setState({
            updateFlatList: !this.state.updateFlatList,
        })

        /*
        selected day Object {
            "dateString": "2020-02-21",
            "day": 21,
            "month": 2,
            "timestamp": 1582243200000,
            "year": 2020,
        }*/

        /*
         Object {
            "barbiere": "Marco",
            "cellulare": "3519727485",
            "cliente": "Manuel",
            "data_prenotazione": "2020-02-28",
            "orario_fine": "13:30",
            "orario_inizio": "13:00",
            "tipo_taglio": "Taglio - Shampoo",
        },*/
    }

    /// GUI /////
    render() {
        var { height, width } = Dimensions.get('window');
        let currentDate = new Date();
        var currentDateString = currentDate.getFullYear().toString() + '-' + currentDate.getMonth().toString() + '-' + currentDate.getDate().toString();

        var marginTopModal = Platform.OS === 'ios' ? Constants.statusBarHeight : 0;

        return (
            <View>
                <Button //Tasto selezione data per il taglio

                    buttonStyle={{
                        backgroundColor: 'white',
                        borderBottomWidth: 1,
                        borderColor: 'black',
                        width: '70%',
                        marginLeft: 'auto',
                        marginRight: 'auto',
                        marginTop: height * 0.025,
                        justifyContent: 'flex-start',
                    }}
                    containerStyle={{
                    }}
                    titleStyle={{
                        color: 'black',
                        textAlign: 'left',
                        fontWeight: '100',
                        fontSize: height * 0.02,
                        marginLeft: 10
                    }}

                    disabled={this.props.selected !== '0' && this.props.taglioSelezionato !== '' ? false : true}

                    onPress={() => {
                        if (this.props.selected !== '0' && this.props.taglioSelezionato !== '') {
                            this.setState({
                                modalSelezioneDataVisibility: true,
                                viewSelezioneDataVisibility: true,
                                selectedDateString: '',
                                selectedDay: '',
                                selectedMonth: '',
                                selectedYear: '',
                                viewSelezioneOrarioVisibility: false,
                                prenotazioni: [],
                                orariDisponibili: [],
                                updateFlatList: true,
                                selectedOrario: '',
                            })
                        } else if (this.props.selected === '0') {
                        } else {
                        }
                    }}

                    icon={
                        <EvilIcons
                            name="calendar"
                            size={height * 0.03}
                            color="black"
                        />
                    }
                    title={this.props.dataTaglio === '' ? "Seleziona data taglio" : (this.props.dataTaglio) + " alle " + (this.props.orarioTaglio)}

                />
                {/* --------------- MODAL CALENDARIO SELEZIONE DATA ------------- */}
                <Modal
                    animationType="fade"
                    transparent={true}
                    visible={this.state.modalSelezioneDataVisibility}
                    onRequestClose={() => {
                        this.setState({
                            modalSelezioneDataVisibility: false,
                        });
                    }}
                >
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: 'white',
                            marginTop: marginTopModal,
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row'
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: height * 0.0275,
                                    fontWeight: 'bold',
                                    padding: '2%'
                                }}
                            >
                                {"Seleziona " + (this.state.viewSelezioneDataVisibility ? "data taglio" : "orario taglio")}
                            </Text>
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    this.setState({
                                        modalSelezioneDataVisibility: false,
                                    });
                                }}
                            >
                                <EvilIcons
                                    name='close'
                                    size={height * 0.05}
                                    style={{
                                        marginRight: width * 0.025,
                                        marginTop: width * 0.025,
                                        marginLeft: 'auto',
                                    }}
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        {this.state.viewSelezioneDataVisibility &&
                            <Calendar
                                // Initially visible month. Default = Date()
                                current={this.state.currentDate}
                                // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                                minDate={new Date()}
                                // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                                //maxDate={'2012-05-30'}
                                // Handler which gets executed on day press. Default = undefined
                                onDayPress={(day) => {
                                    console.log('selected day', day);
                                    this.setState({
                                        viewSelezioneDataVisibility: false,
                                        viewSelezioneOrarioVisibility: true,
                                        selectedDateString: day.dateString,
                                        selectedDay: day.day < 10 ? '0' + day.day.toString() : day.day.toString(),
                                        selectedMonth: day.month < 10 ? '0' + day.month.toString() : day.month.toString(),
                                        selectedYear: day.year.toString(),
                                    }, () => {
                                        this.getPrenotazioni();
                                    });

                                }}
                                // Handler which gets executed on day long press. Default = undefined
                                //onDayLongPress={(day) => { console.log('selected day', day) }}
                                // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                                monthFormat={'MMMM yyyy'}
                                // Handler which gets executed when visible month changes in calendar. Default = undefined
                                //onMonthChange={(month) => { console.log('month changed', month) }}
                                // Hide month navigation arrows. Default = false
                                hideArrows={false}
                                // Replace default arrows with custom ones (direction can be 'left' or 'right')
                                //renderArrow={(direction) => (<Arrow />)}
                                // Do not show days of other months in month page. Default = false
                                hideExtraDays={false}
                                // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                                // day from another month that is visible in calendar page. Default = false
                                disableMonthChange={false}
                                // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                                firstDay={1}
                                // Hide day names. Default = false
                                hideDayNames={false}
                                // Show week numbers to the left. Default = false
                                showWeekNumbers={false}
                                // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                                onPressArrowLeft={substractMonth => substractMonth()}
                                // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                                onPressArrowRight={addMonth => addMonth()}
                                // Disable left arrow. Default = false
                                disableArrowLeft={false}
                                // Disable right arrow. Default = false
                                disableArrowRight={false}

                                onMonthChange={(date) => {
                                    console.log();

                                    this.setState({
                                        currentDate: date.dateString,
                                        markedDates: this.getDaysInMonth(date.month - 1, date.year)
                                    })
                                }}

                                markedDates={
                                    this.state.markedDates
                                }
                                theme={{
                                    todayTextColor: '#007AFF'
                                }}
                            />}
                        { /* SELEZIONE ORARI */
                            this.state.viewSelezioneOrarioVisibility &&
                            <View
                                style={{
                                    flex: 1,
                                }}
                            >
                                <View
                                    style={{
                                        height: '80%'
                                    }}
                                >
                                    <FlatList
                                        style={{
                                            //borderWidth: 2,
                                            //borderColor: 'black',
                                        }}
                                        contentContainerStyle={{
                                            alignContent: 'center',
                                            alignItems: 'center',
                                        }}
                                        keyExtractor={item => item.inizio}
                                        showsVerticalScrollIndicator={true}
                                        bounces={false}
                                        numColumns={4}
                                        data={this.state.orariDisponibili}
                                        renderItem={({ item, index }) => (
                                            <View
                                                style={{
                                                    padding: '2%',
                                                    borderColor: 'black',
                                                    borderWidth: 0,
                                                    borderRadius: 25,
                                                }}
                                            >
                                                <Button
                                                    //activeOpacity={0.5}
                                                    onPress={() => {
                                                        console.log('Clicked');
                                                        this.setState({
                                                            selectedOrario: item.inizio + "-" + item.fine
                                                        }, () => {
                                                            console.log(this.state.selectedOrario);
                                                            this.setState({
                                                                updateFlatList: !this.state.updateFlatList,
                                                            })
                                                        })
                                                    }}
                                                    buttonStyle={[{
                                                        width: width / 5,
                                                        height: width / 5,
                                                        borderRadius: 25,
                                                        alignContent: 'center',
                                                        justifyContent: 'center',
                                                        alignItems: 'center',
                                                        shadowColor: "#000",
                                                        shadowOffset: {
                                                            width: 0,
                                                            height: 3,
                                                        },
                                                        shadowOpacity: 0.27,
                                                        shadowRadius: 4.65,

                                                        elevation: 10,
                                                    }, this.state.selectedOrario.split('-')[0] !== item.inizio && this.state.selectedOrario.split('-')[1] !== item.fine ?
                                                        {
                                                            borderColor: 'black',
                                                            borderWidth: 1,
                                                            backgroundColor: 'white',
                                                        } : {
                                                            borderColor: 'black',
                                                            borderWidth: 0,
                                                            backgroundColor: 'black',
                                                        }]}
                                                    title={"Dalle " + item.inizio + "\nalle " + item.fine}
                                                    titleStyle={[{
                                                        fontSize: width * 0.025,
                                                        textAlign: 'center'
                                                    }, this.state.selectedOrario.split('-')[0] !== item.inizio && this.state.selectedOrario.split('-')[1] !== item.fine ?
                                                        {
                                                            color: 'black',
                                                        } : {
                                                            color: 'white',
                                                        }
                                                    ]}
                                                />
                                            </View>
                                        )}
                                        extraData={this.state.updateFlatList}
                                    />
                                </View>
                                {/* BOTTONE AVANTI */}
                                <Button
                                    title={"AVANTI"}
                                    buttonStyle={{
                                        width: '30%',
                                        backgroundColor: 'black',
                                        borderRadius: 100,
                                        marginLeft: 'auto',
                                        marginRight: width * 0.03,
                                        marginTop: height * 0.025
                                    }}
                                    titleStyle={{
                                        fontSize: height * 0.02,
                                        fontWeight: 'bold',
                                        color: 'white'
                                    }}
                                    disabled={this.state.selectedOrario !== '' ? false : true}
                                    onPress={() => {
                                        this.setState({
                                            modalSelezioneDataVisibility: false,
                                        })
                                        this.props.setDate(this.state.selectedYear,
                                            this.state.selectedMonth,
                                            this.state.selectedDay,
                                            this.state.selectedOrario.split('-')[0],
                                            this.state.selectedOrario.split('-')[1]);
                                    }}
                                />
                            </View>
                        }
                    </View>
                </Modal>
                {/* --------------- FINE MODAL CALENDARIO SELEZIONE DATA ------------- */}
            </View >
        );
    }
}