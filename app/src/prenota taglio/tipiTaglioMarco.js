const tipiTaglioMarco = [{
    nome: "Taglio + Shampoo",
    durata: 30,
    costo: "15€",
    id: '0'
},
{
    nome: "Taglio + Shampoo + Barba",
    durata: 45,
    costo: "22€",
    id: '1'
},
{
    nome: "Barba + Trattamento",
    durata: 20,
    costo: "10€",
    id: '2'
},
{
    nome: "Taglio Bimbo fino a 6 anni",
    durata: 20,
    costo: "12€",
    id: '3'
}
]

export default tipiTaglioMarco;