import React from 'react';
import { Dimensions, AsyncStorage, StyleSheet, Text, View, StatusBar, Platform, SafeAreaView, Image, TouchableWithoutFeedback, BackHandler, Modal, TouchableHighlight, FlatList, ScrollView, Alert } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign } from '@expo/vector-icons';
import tipiTaglioManuel from './tipiTaglioManuel';
import tipiTaglioMarco from './tipiTaglioMarco';
import moment from 'moment';
import localization from 'moment/locale/it'
import SelezioneData from './selezioneData'
import ModalTipoTaglio from './modalTipoTaglio'
import { Button } from 'react-native-elements'
import Toast, { DURATION } from 'react-native-easy-toast'
import firebaseAppInitialized from '../firebase/firebase'
import LoadModal from '../loadModal/LoadModal'

export default class PrenotaTaglio extends React.Component {

    constructor() {
        super();

        this.state = {
            selected: '0',
            modelSelezioneTipoTaglioVisibility: false,
            taglioSelezionato: '',
            durataTaglio: '',
            tipiTaglio: [],
            dataTaglio: '',
            orarioTaglio: '',
            modelSelezioneDataTaglioVisibility: false,
            modelSelezioneDataTaglioMode: 'date',
            loadingModal: {
                loading: false,
                titolo: '',
            }
        }
    }

    async componentDidMount() {
        var user = await firebaseAppInitialized.auth().currentUser;
        var facebookToken = '';
        var rimaniConnesso = '';

        await AsyncStorage.getItem('facebookToken', (error, result) => {
            facebookToken = result;
            console.log('facebookToken : ' + facebookToken);
        });

        await AsyncStorage.getItem('rimaniConnesso', (error, result) => {
            rimaniConnesso = result;
            console.log('Rimani connesso : ' + rimaniConnesso);
        })

        user === null ? console.log('User non loggato') : console.log('User loggato');

        if (user || (facebookToken !== null && facebookToken !== '') || (rimaniConnesso !== null && rimaniConnesso !== 'false')) {
            // User is signed in.
            console.log('Utente Loggato');

        } else {
            // No user is signed in.
            console.log('Nessun utente, prima loggarsi');
            Alert.alert(
                'Non hai effettuato l\'accesso!',
                'Per poter prenotare un taglio devi prima effettuare l\'accesso',
                [
                    {
                        text: 'Vai alla home',
                        onPress: () => this.props.navigation.navigate('Benvenuto'),
                        style: 'cancel',
                    },
                    { text: 'Vai al login', onPress: () => this.props.navigation.navigate('Login') },
                ],
                { cancelable: false }
            );
        }
    }

    prenotaTaglio() {

    }

    getOrariTagli = async (tipiTaglioTitle) => {

        console.log('Prendo orari...');
        var tipiTaglio = this.state.tipiTaglio;

        firebaseAppInitialized.auth().app.firestore()
            .collection(tipiTaglioTitle).get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    // doc.data() is never undefined for query doc snapshots
                    console.log(doc.id, " => ", doc.data());
                    tipiTaglio.push({
                        nome: doc.id,
                        durata: doc.data().durata,
                        costo: doc.data().costo,
                        id: doc.id,
                    });
                });
            })
            .then(() => {
                this.setState({
                    tipiTaglio: tipiTaglio,
                    loadingModal: {
                        loading: false,
                        titolo: '',
                    }
                })
            })
            .catch((error) => {
                console.log("Error getting documents: ", error);
            });
    }

    selectTaglio = (tipoTaglio, durata) => {

        this.setState({
            taglioSelezionato: tipoTaglio,
            durataTaglio: durata,
            modelSelezioneTipoTaglioVisibility: false,
        })

        console.log('Selezionato : ' + tipoTaglio);
    }

    closeModal = () => {
        this.setState({
            modelSelezioneTipoTaglioVisibility: false
        })
        console.log('Chiudo modal');

    }

    setDate = (anno, mese, giorno, orarioInizioTaglio, orarioFineTaglio) => {

        //console.log(event);

        //console.log(date);

        moment.locale('it');
        var dateString = '';
        var timeString = '';
        //dateString = moment(date).locale('it', localization).format('DD/MM/YY');
        //timeString = moment(date).locale('it', localization).format('HH:mm');

        this.setState({
            modelSelezioneDataTaglioVisibility: false,
            dataTaglio: anno + '-' + mese + '-' + giorno,
            orarioTaglio: orarioInizioTaglio + '-' + orarioFineTaglio, //Da splittare quando si carica su DB
        }, () => {
            console.log(this.state.dataTaglio + ' - ' + this.state.orarioTaglio);
        });


        //LOGICA PER VERIFICARE LA DISPONIBILITA' DELLA DATA
    }

    cancelSetDate = () => {
        this.setState({
            modelSelezioneDataTaglioVisibility: false,
            modelSelezioneDataTaglioMode: 'date',
            dataTaglio: '',
            orarioTaglio: '',
        });
    }

    prenotaTaglio = () => {
        //LOGICA PRENOTAZIONE
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View
                style={{
                    height: "90%",
                    alignContent: 'center',
                }}
            >
                <ScrollView
                    style={{
                        height: "90%",
                    }}
                    containerStyle={{
                        flexGrow: 1,
                    }}
                    horizontal={false}
                    scrollEnabled={true}
                    behaviour="height"
                    bounces={false}
                >
                    <View
                        style={{
                            paddingBottom: 50
                        }}
                    >
                        {/* -------- MODAL LOAD -------------- */}
                        <LoadModal visible={this.state.loadingModal.loading} titolo={this.state.loadingModal.titolo} />

                        {/* -------- MODAL SELEZIONE TIPO TAGLIO -------------- */}
                        <ModalTipoTaglio
                            modelSelezioneTipoTaglioVisibility={this.state.modelSelezioneTipoTaglioVisibility}
                            closeModal={this.closeModal}
                            selectTaglio={this.selectTaglio}
                            tipiTaglio={this.state.tipiTaglio}
                        />
                        {/* -------- FINE MODAL SELEZIONE TIPO TAGLIO -------------- */}

                        <Image
                            source={require('../../res/logo_orizzontale.png')}
                            style={{ width: width, height: height * 0.25 }}
                            resizeMode='contain'
                        />

                        {/* ----------- SELEZIONE BARBER ---------------*/}
                        <Text style={{ fontSize: height * 0.02, fontWeight: 'bold', marginLeft: width * 0.03, marginTop: height * 0.03 }}>
                            {"Scegli il barbiere per il taglio"}
                        </Text>

                        <View style={{ flexDirection: 'row' }}>
                            <TouchableWithoutFeedback //Barber 1 - Marco
                                onPress={() => {
                                    console.log('cliccato barber 1');
                                    this.setState({
                                        selected: '1',
                                        tipiTaglio: [],
                                        modelSelezioneTipoTaglioVisibility: false,
                                        taglioSelezionato: '',
                                        modelSelezioneDataTaglioVisibility: false,
                                        modelSelezioneDataTaglioMode: 'date',
                                        dataTaglio: '',
                                        orarioTaglio: '',
                                        loadingModal: {
                                            loading: true,
                                            titolo: 'Caricamento orari',
                                        }
                                    }, () => {
                                        this.getOrariTagli('Tagli Marco');
                                    });
                                }
                                }
                            >
                                <View style={{
                                    width: 75,
                                    height: 75,
                                    marginLeft: width * 0.2,
                                    marginTop: height * 0.025,
                                    alignContent: 'center',
                                    alignItems: 'center'
                                }}
                                >
                                    <View style={{
                                        width: 75,
                                        height: 75,
                                        borderRadius: 100,
                                        borderWidth: 1,
                                        alignContent: 'center',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        borderColor: this.state.selected === '1' ? '#007AFF' : 'black',
                                    }}
                                    >
                                        <Image
                                            source={require('../../res/user.png')}
                                            style={{ width: 65, height: 65, borderRadius: 2000 }}
                                            resizeMode='contain'
                                        />
                                    </View>
                                    <Text style={{
                                        color: this.state.selected === '1' ? '#007AFF' : 'black',
                                    }}>Marco</Text>
                                </View>
                            </TouchableWithoutFeedback>

                            <TouchableWithoutFeedback //BARBER 2 - MANUEL
                                onPress={() => {
                                    console.log('cliccato barber 2');
                                    this.setState({
                                        selected: '2',
                                        modelSelezioneTipoTaglioVisibility: false,
                                        taglioSelezionato: '',
                                        tipiTaglio: [],
                                        modelSelezioneDataTaglioVisibility: false,
                                        modelSelezioneDataTaglioMode: 'date',
                                        dataTaglio: '',
                                        orarioTaglio: '',
                                        loadingModal: {
                                            loading: true,
                                            titolo: 'Caricamento orari',
                                        }
                                    }, () => {
                                        this.getOrariTagli('Tagli Manuel');
                                    });
                                }
                                }
                            >
                                <View style={{
                                    width: 75,
                                    height: 75,
                                    marginLeft: width * 0.2,
                                    marginTop: height * 0.025,
                                    alignContent: 'center',
                                    alignItems: 'center'
                                }}
                                >
                                    <View style={{
                                        width: 75,
                                        height: 75,
                                        borderRadius: 100,
                                        borderWidth: 1,
                                        alignContent: 'center',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        borderColor: this.state.selected === '2' ? '#007AFF' : 'black',
                                    }}
                                    >
                                        <Image
                                            source={require('../../res/user.png')}
                                            style={{ width: 65, height: 65, borderRadius: 2000 }}
                                            resizeMode='contain'
                                        />
                                    </View>
                                    <Text style={{
                                        color: this.state.selected === '2' ? '#007AFF' : 'black',
                                    }}>Manuel</Text>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                        {/* ----------- FINE SELEZIONE BARBER ---------------*/}
                        {/* ----------- SELEZIONE TIPO TAGLIO ---------------*/}
                        <Text style={{ fontSize: height * 0.02, fontWeight: 'bold', marginLeft: width * 0.03, marginTop: height * 0.05 }}>
                            {"Scegli il tipo di taglio"}
                        </Text>

                        <Button //Tasto selezione data per il taglio

                            buttonStyle={{
                                backgroundColor: 'white',
                                borderBottomWidth: 1,
                                borderColor: 'black',
                                width: '70%',
                                marginLeft: 'auto',
                                marginRight: 'auto',
                                marginTop: height * 0.025,
                                justifyContent: 'flex-start',
                            }}
                            containerStyle={{
                            }}
                            titleStyle={{
                                color: 'black',
                                textAlign: 'left',
                                fontWeight: '100',
                                fontSize: height * 0.02,
                                marginLeft: 10
                            }}

                            disabled={this.state.selected !== '0' ? false : true}

                            onPress={() => {
                                if (this.state.selected !== '0') {
                                    this.setState({
                                        modelSelezioneTipoTaglioVisibility: true
                                    })
                                }
                            }}

                            icon={
                                <EvilIcons
                                    name="calendar"
                                    size={height * 0.03}
                                    color="black"
                                />
                            }
                            title={this.state.taglioSelezionato === '' ? "Seleziona taglio" : this.state.taglioSelezionato}
                        />


                        {/* ----------- FINE SELEZIONE TIPO TAGLIO ---------------*/}
                        {/* ----------- SELEZIONE DATA TAGLIO ---------------*/}
                        <Text style={{ fontSize: height * 0.02, fontWeight: 'bold', marginLeft: width * 0.03, marginTop: height * 0.035 }}>
                            {"Scegli la data per il taglio"}
                        </Text>
                        <SelezioneData
                            setDate={this.setDate}
                            modelSelezioneDataTaglioMode={this.state.modelSelezioneDataTaglioMode}
                            modelSelezioneDataTaglioVisibility={this.state.modelSelezioneDataTaglioVisibility}
                            dataTaglio={this.state.dataTaglio}
                            orarioTaglio={this.state.orarioTaglio}
                            selected={this.state.selected}
                            taglioSelezionato={this.state.taglioSelezionato}
                            durataTaglio={this.state.durataTaglio}
                            cancelSetDate={this.cancelSetDate}
                            showDatePicker={() => {
                                this.setState({
                                    modelSelezioneDataTaglioVisibility: true
                                })
                            }}
                        />
                        {/* ----------- FINE SELEZIONE DATA TAGLIO ---------------*/}
                        {/* ----------- TASTO PRENOTA TAGLIO ---------------*/}

                        <Button
                            buttonStyle={{
                                width: '30%',
                                backgroundColor: 'black',
                                borderRadius: 100,
                                marginLeft: 'auto',
                                marginRight: width * 0.03,
                                marginTop: height * 0.035
                            }}
                            title={"PRENOTA"}
                            titleStyle={{
                                fontSize: height * 0.02,
                                fontWeight: 'bold',
                                color: 'white'
                            }}
                            onPress={async () => {
                                // EFFETTUO PRENOTAZIONE
                                // IMPORTANTE - CONTROLLO PRIMA CHE L'ORARIO NON SIA STATO OCCUPATO NEL MENTRE
                                console.log('Prenotazione');
                                this.refs.toast.show('  Prenotazione...  ', 750);
                                var user = await firebaseAppInitialized.auth().currentUser;
                                var barbiere = this.state.selected === '1' ? 'Marco' : 'Manuel';
                                var telefonoCliente = '';
                                var nomeCliente = '';
                                var dataTaglio = this.state.dataTaglio;
                                var orarioInizo = this.state.orarioTaglio.split('-')[0];
                                var orarioFine = this.state.orarioTaglio.split('-')[1];
                                var tipoTaglio = this.state.taglioSelezionato;

                                if (user) {
                                    await firebaseAppInitialized.auth().app.firestore().collection('Utenti').doc(user.email).get()
                                        .then((doc) => {
                                            console.log(doc.data());
                                            telefonoCliente = doc.data().cellulare;
                                            nomeCliente = doc.data().nomeCognome;
                                        })
                                }

                                console.log(this.state.orarioTaglio.split('-'));



                                firebaseAppInitialized.auth().app.firestore().collection('Prenotazioni').doc()
                                    .set({
                                        barbiere: barbiere,
                                        cellulare: telefonoCliente,
                                        cliente: nomeCliente,
                                        email: user.email,
                                        data_prenotazione: dataTaglio,
                                        orario_fine: orarioFine,
                                        orario_inizio: orarioInizo,
                                        tipo_taglio: tipoTaglio,
                                        eliminata: false,
                                        effettuata: false,
                                    }).then(() => {

                                        // INVIO NOTIFICA ALL' ADMIN/BARBIERE SELEZIONATO

                                        var adminNotificationToken = '';
                                        var mailBarberSelezionato = '';

                                        if (this.state.selected === '1') {
                                            // EMAIL MARCO
                                            mailBarberSelezionato = 'manuelmelaragno@gmail.com'

                                        } else {
                                            // EMAIL MANUEL
                                            mailBarberSelezionato = 'manuelmelaragno@gmail.com'

                                        }

                                        //PRENDO IL TOKEN DA FIREBASE
                                        firebaseAppInitialized.auth().app.firestore().collection('Utenti')
                                            .where('admin', '==', true).where('email', '==', mailBarberSelezionato)
                                            .get()
                                            .then(async (querySnapshot) => {
                                                querySnapshot.forEach(async (doc) => {
                                                    // doc.data() is never undefined for query doc snapshots
                                                    console.log(doc.id, " => ", doc.data());
                                                    var ExponentPushToken = doc.data().ExponentPushToken;
                                                    dataTaglio = dataTaglio.split('-')[2] + '/' + dataTaglio.split('-')[1] + '/' + dataTaglio.split('-')[0];

                                                    //INVIO NOTIFICA
                                                    const message = {
                                                        to: ExponentPushToken,
                                                        sound: 'default',
                                                        title: 'Nuova Prenotazione',
                                                        body: 'Prenotazione da ' + nomeCliente + '\nIn data ' + dataTaglio + ' ' + orarioInizo + '-' + orarioFine,
                                                        data: { data: 'goes here' },
                                                        _displayInForeground: true,
                                                    };
                                                    const response = await fetch('https://exp.host/--/api/v2/push/send', {
                                                        method: 'POST',
                                                        headers: {
                                                            Accept: 'application/json',
                                                            'Accept-encoding': 'gzip, deflate',
                                                            'Content-Type': 'application/json',
                                                        },
                                                        body: JSON.stringify(message),
                                                    }).then(() => {
                                                        console.log('Notifica inviata');
                                                    }).catch((errors) => {
                                                        console.log('Errore notifica');
                                                        console.log(errors);

                                                    });
                                                });
                                            })
                                            .catch(function (error) {
                                                console.log("Error getting documents: ", error);
                                            });


                                        console.log('Prenotazione effettuta');
                                        this.refs.toast.show('  Prenotazione effettuta  ', 750);
                                    })

                            }}
                            disabled={this.state.orarioTaglio !== '' ? false : true}
                        />

                        {/* ----------- FINE TASTO PRENOTA TAGLIO ---------------*/}
                    </View>
                </ScrollView>
                <Toast
                    ref="toast"
                    fadeOutDuration={250}
                    fadeInDuration={250}
                    textStyle={{
                        fontWeight: '100',
                        color: 'white'
                    }}
                    style={{
                        borderRadius: 100,
                    }}
                    positionValue={height * 0.25}
                />
            </View>
        );
    }
}
