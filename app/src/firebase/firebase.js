import * as firebase from 'firebase';
import 'firebase/firestore';

//Configurazione per FireBase
const firebaseAppInitialized = firebase.initializeApp({
  // copy and paste your firebase credential here
  apiKey: "AIzaSyCbDeSNP6m6Nf3J5-lzCJAVJo-g8USZv94",
  authDomain: "barber-app-496a2.firebaseapp.com",
  databaseURL: "https://barber-app-496a2.firebaseio.com",
  projectId: "barber-app-496a2",
  storageBucket: "barber-app-496a2.appspot.com",
  messagingSenderId: "464144754942",
  appId: "1:464144754942:web:b9f6497b1c698809db9daf",
  measurementId: "G-S5H4ZE8XN6"
});

export default firebaseAppInitialized;
