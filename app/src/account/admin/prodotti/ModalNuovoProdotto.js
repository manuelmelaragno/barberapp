import React from 'react';
import { Dimensions, StyleSheet, Text, Modal, View, StatusBar, Platform, AsyncStorage, SafeAreaView, Image, TextInput, Linking, ScrollView, BackHandler, Alert, TouchableHighlight, TouchableOpacity, Keyboard } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons, Ionicons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Button } from 'react-native-elements'
import ModalTipoTaglio from '../../../prenota taglio/modalTipoTaglio';
import tipiTaglioManuel from '../../../prenota taglio/tipiTaglioManuel';
import tipiTaglioMarco from '../../../prenota taglio/tipiTaglioMarco';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

export default class ModalNuovoProdotto extends React.Component {

    constructor() {
        super();

        this.state = {
            imageLocal: null,
            nomeProdotto: '',
            descrizioneProdotto: '',
        };
    }

    async componentDidMount() {
    }

    componentWillUnmount() {
    }

    getPermissionsCamera = async () => {
        if (Constants.isDevice) {
            const { status: existingStatus } = await Permissions.getAsync(Permissions.CAMERA);
            let finalStatus = existingStatus;
            if (existingStatus !== 'granted') {
                const { status } = await Permissions.askAsync(Permissions.CAMERA);
                finalStatus = status;
            }
            if (finalStatus !== 'granted') {
                Alert.alert('Bisogna consentire i permessi per accedere alla fotocamera');
                return;
            }
            console.log(finalStatus);

        } else {
            Alert.alert('Bisogna usare un telefono fisico');
        }
    }

    getPermissionsCameraRoll = async () => {
        if (Constants.isDevice) {
            const { status: existingStatus } = await Permissions.getAsync(Permissions.CAMERA_ROLL);
            let finalStatus = existingStatus;
            if (existingStatus !== 'granted') {
                const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
                finalStatus = status;
            }
            if (finalStatus !== 'granted') {
                Alert.alert('Bisogna consentire i permessi per accedere alla libreria');
                return;
            }
            console.log(finalStatus);

        } else {
            Alert.alert('Bisogna usare un telefono fisico');
        }
    }

    caricaFoto = async () => {
        await this.getPermissionsCamera();
        await this.getPermissionsCameraRoll();
        try {
            let result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                allowsEditing: true,
                aspect: [4, 3],
                quality: 0.1,
            });

            if (!result.cancelled) {
                this.setState({ imageLocal: result.uri });
            }

            console.log(result);
        } catch (E) {
            console.log(E);
        }

    }

    aggiungiProdotto = async () => {
        if (this.state.imageLocal !== '' && this.state.imageLocal !== null && this.state.nomeProdotto !== '') {
            var downloadUrl = '';
            var nomeProdotto = this.state.nomeProdotto;
            var descrizioneProdotto = this.state.descrizioneProdotto;

            // AGGIUNGO PRODOTTO AL DB
            console.log('Aggiungo...');
            //const response = await fetch(this.state.imageLocal);
            const blob = await new Promise((resolve, reject) => {
                const xhr = new XMLHttpRequest();
                xhr.onload = function () {
                    resolve(xhr.response);
                };
                xhr.onerror = function (e) {
                    console.log(e);
                    reject(new TypeError('Network request failed'));
                };
                xhr.responseType = 'blob';
                xhr.open('GET', this.state.imageLocal, true);
                xhr.send(null);
            });
            console.log('Aggiungo a firebase');
            await firebaseAppInitialized.auth().app.storage().ref().child("prodotti/" + this.state.nomeProdotto).put(blob);
            await firebaseAppInitialized.auth().app.storage().ref().child("prodotti/" + this.state.nomeProdotto).getDownloadURL().then((url) => { downloadUrl = url });
            console.log('Download URL : ' + downloadUrl);
            blob.close();

            await firebaseAppInitialized.auth().app.firestore().collection('Prodotti').doc()
                .set({
                    nome: nomeProdotto,
                    uriFoto: downloadUrl,
                    descrizione: descrizioneProdotto,
                }).then(() => {
                    this.props.hide(true);
                })
        }
    }
    /*
        _urlToBlob = (url) => {
            return new Promise((resolve, reject) => {
                var xhr = new XMLHttpRequest();
                xhr.onerror = reject;
                xhr.onreadystatechange = () => {
                    if (xhr.readyState === 4) {
                        resolve(xhr.response);
                    }
                };
                xhr.open('GET', url);
                xhr.responseType = 'blob'; // convert type
                xhr.send();
            })
        }
    */

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <Modal
                animationType="slide" // Default- slide
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => {
                    this.props.hide(false);
                }}>
                <View style={{ marginTop: Platform.OS === 'ios' ? Constants.statusBarHeight : 0, flex: 1, backgroundColor: 'white', padding: width * 0.03, }}>
                    <KeyboardAwareScrollView>
                        {/* BOTTONE INDIETRO */}
                        <TouchableOpacity
                            style={{
                                width: width * 0.3,
                            }}
                            onPress={() => {
                                this.props.hide(false);
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderWidth: 0,
                                }}
                            >
                                <Ionicons
                                    name='ios-arrow-back'
                                    size={width * 0.1}
                                    color={'#007AFF'}
                                />
                                <Text
                                    style={{
                                        borderWidth: 0,
                                        alignSelf: 'center',
                                        color: '#007AFF',
                                        fontSize: width * 0.05,
                                        marginLeft: width * 0.03,
                                        fontWeight: '100'
                                    }}
                                >
                                    {'Indietro'}
                                </Text>
                            </View>
                        </TouchableOpacity>
                        {/* TITOLO */}
                        <Text
                            style={{
                                fontSize: width * 0.075,
                                marginTop: height * 0.02,
                                fontWeight: 'bold',
                            }}
                        >
                            {"Aggiungi prodotto"}
                        </Text>

                        {/* IMMAGINE PRODOTTO */}
                        <Text
                            style={{
                                marginTop: height * 0.02,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Foto prodotto'}
                        </Text>
                        <TouchableOpacity
                            style={{
                                borderWidth: 1,
                                borderRadius: 100,
                                width: width * 0.3,
                                height: width * 0.3,
                                marginTop: width * 0.05,
                                justifyContent: 'center',
                                alignContent: 'center',
                                alignItems: 'center'
                            }}
                            onPress={this.caricaFoto}
                        >
                            {this.state.imageLocal == null && <Ionicons name='ios-camera' size={width * 0.1} />}
                            {this.state.imageLocal && <Image source={{ uri: this.state.imageLocal }} style={{ width: '100%', height: '100%', borderRadius: 100 }} />}
                        </TouchableOpacity>
                        {/* NOME PRODOTTO*/}
                        <Text
                            style={{
                                marginTop: height * 0.03,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Nome prodotto'}
                        </Text>
                        <TextInput
                            style={{
                                width: width,
                                height: height * 0.05,
                                borderWidth: 0,
                                fontSize: width * 0.05,
                                textAlign: 'left',
                                marginTop: height * 0.01,
                                borderBottomWidth: 0.5,
                            }}
                            onChangeText={(text) => {
                                this.setState({ nomeProdotto: text })
                            }}
                            onSubmitEditing={() => {

                            }}
                            keyboardType='default'
                            returnKeyLabel='Done'
                            returnKeyType='done'
                            value={this.state.nomeProdotto}
                            placeholder='Nome'
                            selectTextOnFocus
                            ref={ref => this.nomeProdotto = ref}
                        />
                        {/* DESCRIZIONE PRODOTTO*/}
                        <Text
                            style={{
                                marginTop: height * 0.03,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Descrizione prodotto'}
                        </Text>
                        <TextInput
                            style={{
                                width: width,
                                borderWidth: 0,
                                fontSize: width * 0.05,
                                textAlign: 'left',
                                marginTop: height * 0.01,
                                borderBottomWidth: 0.5,
                                paddingBottom: width * 0.01,
                            }}
                            onChangeText={(text) => {
                                this.setState({ descrizioneProdotto: text })
                            }}
                            onSubmitEditing={() => {

                            }}
                            keyboardType='default'
                            value={this.state.descrizioneProdotto}
                            placeholder='Descrizione'
                            selectTextOnFocus
                            multiline={true}
                            ref={ref => this.nomeProdotto = ref}
                        />
                        {/* TASTO AGGIUNGI*/}
                        <TouchableOpacity
                            style={{
                                borderWidth: 1,
                                width: width * 0.50,
                                height: height * 0.075,
                                borderRadius: 100,
                                backgroundColor: 'black',
                                marginRight: 0,
                                marginLeft: 'auto',
                                justifyContent: 'center',
                                marginTop: width * 0.2,
                            }}
                            onPress={this.aggiungiProdotto}
                        >
                            <Text style={{ color: 'white', fontSize: width * 0.05, fontWeight: 'bold', textAlign: 'center' }}>
                                {'AGGIUNGI'}
                            </Text>
                        </TouchableOpacity>
                    </KeyboardAwareScrollView>
                </View>
            </Modal>
        );
    }
}
