import React from 'react';
import { Dimensions, StyleSheet, Text, View, StatusBar, Platform, AsyncStorage, SafeAreaView, Image, Linking, ScrollView, TouchableOpacity, BackHandler, FlatList, RefreshControl, Alert } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons, Ionicons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';
import ModalNuovoProdotto from './ModalNuovoProdotto'

export default class AdminProdotti extends React.Component {

    constructor() {
        super();

        this.state = {
            modalNuovoProdottoVisibility: false, // Default - false
            prodotti: [],
            loadingProdotti: true,
            limitQuery: 15,
            pivot: null,
        };
    }

    async componentDidMount() {
        this.caricaProdotti();
    }

    componentWillUnmount() {

    }

    caricaProdotti = () => {
        console.log('Carico prodotti...');

        var pivot = this.state.pivot;
        var limit = this.state.limitQuery;
        var user = firebaseAppInitialized.auth().currentUser;

        firebaseAppInitialized.auth().app.firestore().collection('Prodotti')
            .orderBy('nome').startAfter(pivot).limit(limit)
            .get()
            .then((querySnapshot) => {
                if (querySnapshot.docs.length > 0) {
                    pivot = querySnapshot.docs[querySnapshot.docs.length - 1];
                    //console.log(pivot);
                    querySnapshot.forEach((doc) => {
                        // doc.data() is never undefined for query doc snapshots
                        console.log(doc.id, " => ", doc.data());

                        var prodotti = this.state.prodotti;
                        prodotti.push({
                            nome: doc.data().nome,
                            uriFoto: doc.data().uriFoto,
                            descrizione: doc.data().descrizione,
                            idProdotto: doc.id,
                        })
                        this.setState({
                            prodotti: prodotti,
                            loadingProdotti: false,
                        })
                    });

                    this.setState({
                        pivot: pivot,
                    })
                }
            })
    }

    eliminaProdotto = (idProdotto, nome) => {
        console.log('Prodotto ' + idProdotto + ' in eleminazione...');
        Alert.alert(
            'Elimina prodotto',
            'Vuoi eliminare il prodotto?',
            [
                {
                    text: 'No',
                    onPress: () => {
                        console.log('Prodotto ' + idProdotto + ' NON eliminato...');
                    },
                    style: 'cancel',
                },
                {
                    text: 'Sì', onPress: () => {
                        // AGGIORNO DB PER L'ELIMINAZIONE DEL PRODOTTO
                        firebaseAppInitialized.auth().app.firestore().collection('Prodotti')
                            .doc(idProdotto).delete()
                            .then(() => {
                                firebaseAppInitialized.auth().app.storage().ref()
                                    .child('prodotti/' + nome).delete()
                                    .then(() => {
                                        var prodotti = this.state.prodotti;

                                        for (var i = 0; i < prodotti.length; i++) {
                                            if (prodotti[i].idProdotto = idProdotto) {
                                                prodotti.splice(i, 1);
                                                this.setState({
                                                    prodotti: prodotti,
                                                })
                                            }
                                        }
                                        console.log('Prodotto ' + idProdotto + ' eliminato...');
                                    })
                            });

                    },
                }
            ],
            { cancelable: false }
        );
    }

    hideModal = (aggiorna) => {
        console.log('Hide modal');

        this.setState({
            modalNuovoProdottoVisibility: false,
        }, () => console.log(this.state.modalNuovoProdottoVisibility))

        if (aggiorna) {
            this.setState({
                pivot: null,
                loadingProdotti: true,
                prodotti: [],
            }, () => {
                this.caricaProdotti();
                this.refs.toast.show('  Prodotto aggiunto con successo  ', 750);
            })
        }
    }

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <View
                style={{
                    height: "90%",
                    paddingLeft: width * 0.03,
                    paddingRight: width * 0.03,
                    paddingTop: width * 0.03,
                }}
            >
                <ModalNuovoProdotto visible={this.state.modalNuovoProdottoVisibility} hide={this.hideModal} />
                <View
                    style={{
                        flexDirection: 'row',
                    }}
                >
                    {/* BOTTONE INDIETRO */}
                    <TouchableOpacity
                        style={{
                            width: width * 0.3,
                        }}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                            }}
                        >
                            <Ionicons
                                name='ios-arrow-back'
                                size={width * 0.1}
                                color={'#007AFF'}
                            />
                            <Text
                                style={{
                                    borderWidth: 0,
                                    alignSelf: 'center',
                                    color: '#007AFF',
                                    fontSize: width * 0.05,
                                    marginLeft: width * 0.03,
                                    fontWeight: '100'
                                }}
                            >
                                {'Indietro'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                    {/* BOTTONE AGGIUNGI */}
                    <TouchableOpacity
                        style={{
                            width: width * 0.15,
                            marginLeft: 'auto',
                            marginRight: 0,
                            alignContent: 'center',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                        onPress={() => {
                            // APRI MODAL NUOVO PRODOTTO
                            this.setState({
                                modalNuovoProdottoVisibility: true,
                            })
                        }}
                    >
                        <Ionicons
                            name='ios-add'
                            size={width * 0.1}
                            color={'#007AFF'}
                        />
                    </TouchableOpacity>
                </View>
                {/* TITOLO */}
                <Text
                    style={{
                        fontSize: width * 0.1,
                        marginTop: height * 0.02,
                        fontWeight: 'bold',
                    }}
                >
                    {"Prodotti"}
                </Text>

                <View
                    style={{
                        marginTop: height * 0.02,
                        borderBottomColor: '#BCBBC1',
                        borderBottomWidth: 1
                    }}
                />

                {/* LISTA PRODOTTI */}
                <FlatList
                    data={this.state.prodotti}
                    renderItem={({ item }) =>
                        <Item nome={item.nome} uriFoto={item.uriFoto} descrizione={item.descrizione} idProdotto={item.idProdotto} eliminaProdotto={this.eliminaProdotto} />
                    }
                    keyExtractor={item => item.idProdotto}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.loadingProdotti}
                            onRefresh={() => {
                                this.setState({
                                    pivot: null,
                                    loadingProdotti: true,
                                    prodotti: [],
                                }, () => {
                                    this.caricaProdotti()
                                })
                            }}
                        />
                    }
                    onEndReachedThreshold={0.3}
                    onEndReached={() => {
                        console.log('Carico altri prodotti...');
                        this.caricaProdotti();
                    }}
                />
                <Toast
                    ref="toast"
                    fadeOutDuration={250}
                    fadeInDuration={250}
                    textStyle={{
                        fontWeight: '100',
                        color: 'white',
                        fontSize: width * 0.04,
                    }}
                    style={{
                        borderRadius: 100,
                    }}
                    positionValue={height * 0.25}
                />
            </View >
        );
    }
}

function Item({ nome, uriFoto, descrizione, idProdotto, eliminaProdotto }) {
    var { height, width } = Dimensions.get('window');
    return (
        <View
            style={{
                borderWidth: 0,
                width: '100%',
                height: height * 0.15,
                marginTop: height * 0.02,
                backgroundColor: '#f7f7f7',
                borderRadius: 5,
            }}
        >
            <View
                style={{
                    flexDirection: 'row'
                }}
            >
                <View
                    style={{
                        borderWidth: 0,
                        borderColor: 'red',
                        width: '30%',
                        height: height * 0.15,
                    }}
                >
                    <Image
                        source={{ uri: uriFoto }}
                        style={{ width: '100%', height: '100%', borderBottomLeftRadius: 5, borderTopLeftRadius: 5, }}
                    />
                </View>
                {/* COLONNA 2 - INFO PRODOTTO */}
                <View
                    style={{
                        borderWidth: 0,
                        borderColor: 'red',
                        width: '60%',
                        height: height * 0.146,
                        paddingLeft: width * 0.02
                    }}
                >
                    <Text
                        style={{
                            fontSize: width * 0.0375,
                            fontWeight: 'bold',
                        }}
                    >
                        {nome}
                    </Text>
                    <Text
                        style={{
                            fontSize: width * 0.0325,
                            color: '#222222',
                            maxHeight: '60%',
                        }}
                    >
                        {descrizione}
                    </Text>

                </View>
                {/* COLONNA 3 - AZIONI PRODOTTO */}
                <View
                    style={{
                        borderWidth: 0,
                        borderColor: 'black',
                        width: '10%',
                        height: height * 0.146,
                        flexDirection: 'row'
                    }}
                >
                    <View
                        style={{
                            borderWidth: 0,
                            borderColor: 'green',
                            width: '100%',
                            height: height * 0.146,
                            justifyContent: 'center',
                            paddingRight: width * 0.02,
                        }}
                    >
                        <TouchableOpacity
                            style={{ marginLeft: 'auto', marginRight: 0 }}
                            onPress={() => {
                                eliminaProdotto(idProdotto, nome);
                            }}
                        >
                            <Ionicons
                                name="ios-close-circle"
                                color="red"
                                size={width * 0.08}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}
