import React from 'react';
import { Dimensions, StyleSheet, Text, View, StatusBar, Platform, AsyncStorage, RefreshControl, Image, Modal, FlatList, TouchableWithoutFeedback, BackHandler, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons, Ionicons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ModalModificaOrario from './ModalModificaOrario'

export default class ModalModificaOrariTagli extends React.Component {

    constructor() {
        super();

        this.state = {
            orariBarberSelezionati: '',
            orari: [],
            modalModificaOrario: false,
            loadingOrari: true,
        };
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    setOrariBarber = (orariBarberSelezionati) => {
        this.setState({
            orariBarberSelezionati: orariBarberSelezionati,
        }, () => {
            this.caricaOrari(orariBarberSelezionati);
        });
    }

    caricaOrari = (orariBarberSelezionati) => {
        var orari = [];
        //Carico orari
        firebaseAppInitialized.auth().app.firestore().collection(orariBarberSelezionati).get().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                // doc.data() is never undefined for query doc snapshots
                //console.log(doc.id, " => ", doc.data());
                orari.push({
                    id: doc.id,
                    costo: doc.data().costo,
                    durata: doc.data().durata,
                })
            });
        }).then(() => {
            this.setState({
                orari: orari,
            }, () => {
                console.log('Orari:');
                console.log(this.state.orari);
                this.setState({
                    loadingOrari: false,
                })
            })
        });
    }

    modificaOrario = (id, costo, durata) => {
        this.setState({
            modalModificaOrario: true
        }, () => {
            this.modalModificaOrario.setOrario(id, costo, durata, this.state.orariBarberSelezionati);
        })
    }

    hide = (aggiorna) => {
        this.setState({
            modalModificaOrario: false,
        }, () => {
            if (aggiorna) {
                this.setState({
                    orari: [],
                    loadingOrari: true,
                }, () => {
                    this.caricaOrari(this.state.orariBarberSelezionati);
                })
            }
        })

    }

    handleBackPress = () => {
        // works best when the goBack is async

        /*this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });*/

        return true;
    }

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <Modal
                animationType="none" //Default - slide
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => {
                    this.props.hide();
                }}>
                <View style={{ marginTop: Platform.OS === 'ios' ? Constants.statusBarHeight : 0, flex: 1, backgroundColor: 'white', padding: width * 0.03, }}>
                    <ModalModificaOrario ref={ref => this.modalModificaOrario = ref} visible={this.state.modalModificaOrario} hide={this.hide} />
                    {/* BOTTONE INDIETRO */}
                    <TouchableOpacity
                        style={{
                            width: width * 0.3,
                        }}
                        onPress={() => {
                            this.props.hide();
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                            }}
                        >
                            <Ionicons
                                name='ios-arrow-back'
                                size={width * 0.1}
                                color={'#007AFF'}
                            />
                            <Text
                                style={{
                                    borderWidth: 0,
                                    alignSelf: 'center',
                                    color: '#007AFF',
                                    fontSize: width * 0.05,
                                    marginLeft: width * 0.03,
                                    fontWeight: '100'
                                }}
                            >
                                {'Indietro'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                    {/* TITOLO */}
                    <Text
                        style={{
                            fontSize: width * 0.075,
                            marginTop: height * 0.02,
                            fontWeight: 'bold',
                        }}
                    >
                        {"Modifica orari"}
                    </Text>

                    <View
                        style={{
                            marginTop: height * 0.02,
                            borderBottomColor: '#BCBBC1',
                            borderBottomWidth: 1
                        }}
                    />

                    <FlatList
                        data={this.state.orari}
                        renderItem={({ item }) =>
                            <Item id={item.id} costo={item.costo} durata={item.durata} modificaOrario={this.modificaOrario} />
                        }
                        keyExtractor={item => item.idPrenotazione}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.loadingOrari}
                                onRefresh={() => {
                                    this.setState({
                                        orari: [],
                                    }, () => {
                                        this.caricaOrari(this.state.orariBarberSelezionati);
                                    })
                                }}
                            />
                        }
                    />
                </View>
            </Modal>
        );
    }
}

function Item({ id, costo, durata, modificaOrario }) {
    var { height, width } = Dimensions.get('window');
    return (
        <View
            style={{
                borderWidth: 0,
                width: '100%',
                height: height * 0.15,
                marginTop: height * 0.02,
                backgroundColor: '#f7f7f7',
                borderRadius: 5,
            }}
        >
            <View
                style={{
                    flexDirection: 'row'
                }}
            >
                {/* COLONNA 2 - INFO PRODOTTO */}
                <View
                    style={{
                        borderWidth: 0,
                        borderColor: 'red',
                        width: '60%',
                        height: height * 0.146,
                        paddingLeft: width * 0.02,
                        justifyContent: 'center'
                    }}
                >
                    <Text
                        style={{
                            fontSize: width * 0.05,
                            fontWeight: 'bold',
                        }}
                    >
                        {id}
                    </Text>
                    <Text
                        style={{
                            fontSize: width * 0.045,
                            color: '#222222',
                            maxHeight: '60%',
                        }}
                    >
                        {'Costo: ' + costo}
                    </Text>
                    <Text
                        style={{
                            fontSize: width * 0.045,
                            color: '#222222',
                            maxHeight: '60%',
                        }}
                    >
                        {'Durata : ' + durata + ' Minuti'}
                    </Text>

                </View>
                {/* COLONNA 3 - AZIONI PRODOTTO */}
                <View
                    style={{
                        borderWidth: 0,
                        borderColor: 'black',
                        width: '15%',
                        height: height * 0.146,
                        flexDirection: 'row',
                        marginLeft: 'auto',
                        marginRight: 0,
                    }}
                >
                    <View
                        style={{
                            borderWidth: 0,
                            borderColor: 'green',
                            width: '100%',
                            height: height * 0.146,
                            justifyContent: 'center',
                            paddingRight: width * 0.02,
                        }}
                    >
                        <TouchableOpacity
                            style={{ marginLeft: 'auto', marginRight: 0 }}
                            onPress={() => {
                                modificaOrario(id, costo, durata);
                            }}
                        >
                            <Ionicons
                                name="ios-create"
                                color="black"
                                size={width * 0.1}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}