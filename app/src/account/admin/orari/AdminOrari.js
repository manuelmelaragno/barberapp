import React from 'react';
import { Dimensions, StyleSheet, Text, View, StatusBar, Platform, AsyncStorage, SafeAreaView, Image, Linking, ScrollView, TouchableOpacity, BackHandler } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons, Ionicons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';
import ModalModificaOrari from './ModalModificaOrariTagli'

export default class AdminOrari extends React.Component {

    constructor() {
        super();

        this.state = {
            modalModificaOrariTagliVisibile: false,
        };
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        this.props.navigation.goBack();
        return true;
    }

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <View
                style={{
                    height: "90%",
                    paddingLeft: width * 0.03,
                    paddingRight: width * 0.03,
                    paddingTop: width * 0.03,
                }}
            >
                <ModalModificaOrari
                    ref={ref => this.modalModificaOrari = ref}
                    visible={this.state.modalModificaOrariTagliVisibile}
                    hide={() => { this.setState({ modalModificaOrariTagliVisibile: false }) }}
                />
                <View
                    style={{
                        flexDirection: 'row',
                    }}
                >
                    {/* BOTTONE INDIETRO */}
                    <TouchableOpacity
                        style={{
                            width: width * 0.3,
                        }}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                            }}
                        >
                            <Ionicons
                                name='ios-arrow-back'
                                size={width * 0.1}
                                color={'#007AFF'}
                            />
                            <Text
                                style={{
                                    borderWidth: 0,
                                    alignSelf: 'center',
                                    color: '#007AFF',
                                    fontSize: width * 0.05,
                                    marginLeft: width * 0.03,
                                    fontWeight: '100'
                                }}
                            >
                                {'Indietro'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                    {/* BOTTONE AGGIUNGI 
                    <TouchableOpacity
                        style={{
                            width: width * 0.15,
                            marginLeft: 'auto',
                            marginRight: 0,
                            alignContent: 'center',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                        onPress={() => {
                            // APRI MODAL NUOVA PRENOTAZIONE
                            this.setState({
                                modalNuovaPrenotazioneVisibility: true,
                            })
                        }}
                    >
                        <Ionicons
                            name='ios-add'
                            size={width * 0.1}
                            color={'#007AFF'}
                        />
                    </TouchableOpacity>*/}
                </View>
                {/* TITOLO */}
                <Text
                    style={{
                        fontSize: width * 0.1,
                        marginTop: height * 0.02,
                        fontWeight: 'bold',
                    }}
                >
                    {"Orari"}
                </Text>

                <View
                    style={{
                        marginTop: height * 0.02,
                        borderBottomColor: '#BCBBC1',
                        borderBottomWidth: 1
                    }}
                />

                {/* SOTTOTITOLO */}
                <Text
                    style={{
                        fontSize: width * 0.055,
                        marginTop: height * 0.04,
                    }}
                >
                    {"Scegli cosa voler fare"}
                </Text>

                {/* BOTTONE PER GESTIRE GLI ORARI DI MARCO  */}
                <TouchableOpacity
                    style={{
                        borderWidth: 0,
                        marginTop: height * 0.075,
                    }}
                    onPress={() => {
                        this.setState({ modalModificaOrariTagliVisibile: true }, () => { this.modalModificaOrari.setOrariBarber('Tagli Marco'); })
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            height: height * 0.075,
                            backgroundColor: 'white',
                            borderRadius: 50,
                            borderWidth: 0.75,
                            borderColor: 'black',
                            alignContent: 'center',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Text
                            style={{
                                fontSize: width * 0.055,
                            }}
                        >
                            {'Gestisci orari tagli Marco'}
                        </Text>
                    </View>
                </TouchableOpacity>

                {/* BOTTONE PER GESTIRE GLI ORARI DI MANUEL  */}
                <TouchableOpacity
                    style={{
                        borderWidth: 0,
                        marginTop: height * 0.075,
                    }}
                    onPress={() => {
                        this.setState({ modalModificaOrariTagliVisibile: true }, () => { this.modalModificaOrari.setOrariBarber('Tagli Manuel'); })
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            height: height * 0.075,
                            backgroundColor: 'white',
                            borderRadius: 50,
                            borderWidth: 0.75,
                            borderColor: 'black',
                            alignContent: 'center',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Text
                            style={{
                                fontSize: width * 0.055,
                            }}
                        >
                            {'Gestisci orari tagli Manuel'}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View >
        );
    }
}
