import React from 'react';
import { Dimensions, TextInput, Text, View, StatusBar, Platform, AsyncStorage, RefreshControl, Image, Modal, FlatList, TouchableWithoutFeedback, BackHandler, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons, Ionicons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';

export default class ModalModificaOrario extends React.Component {

    constructor() {
        super();

        this.state = {
            id: 'titolo',
            costo: '',
            durata: '',
            orariBarberSelezionati: '',
        };
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    setOrario = (id, costo, durata, orariBarberSelezionati) => {
        this.setState({
            id: id,
            costo: costo,
            durata: durata,
            orariBarberSelezionati: orariBarberSelezionati,
        })
    }

    handleBackPress = () => {
        // works best when the goBack is async

        /*this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });*/

        return true;
    }

    modificaOrario = () => {

        firebaseAppInitialized.auth().app.firestore()
            .collection(this.state.orariBarberSelezionati).doc(this.state.id)
            .update({
                costo: this.state.costo + '€',
                durata: parseInt(this.state.durata),
            }).then(() => {
                console.log('Orario modificato');
                this.props.hide(true);
            })

    }

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <Modal
                animationType="none" //Default - slide
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => {
                    this.props.hide(false);
                }}>
                <View style={{ marginTop: Platform.OS === 'ios' ? Constants.statusBarHeight : 0, flex: 1, backgroundColor: 'white', padding: width * 0.03, }}>
                    {/* BOTTONE INDIETRO */}
                    <TouchableOpacity
                        style={{
                            width: width * 0.3,
                        }}
                        onPress={() => {
                            this.props.hide(false);
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                            }}
                        >
                            <Ionicons
                                name='ios-arrow-back'
                                size={width * 0.1}
                                color={'#007AFF'}
                            />
                            <Text
                                style={{
                                    borderWidth: 0,
                                    alignSelf: 'center',
                                    color: '#007AFF',
                                    fontSize: width * 0.05,
                                    marginLeft: width * 0.03,
                                    fontWeight: '100'
                                }}
                            >
                                {'Indietro'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                    {/* TITOLO */}
                    <Text
                        style={{
                            fontSize: width * 0.075,
                            marginTop: height * 0.02,
                            fontWeight: 'bold',
                        }}
                    >
                        {"Modifica orario"}
                    </Text>

                    <View
                        style={{
                            marginTop: height * 0.02,
                            borderBottomColor: '#BCBBC1',
                            borderBottomWidth: 1
                        }}
                    />
                    {/* CORPO MODAL */}
                    {/* MODIFICA ID*/}
                    <Text
                        style={{
                            marginTop: height * 0.04,
                            fontSize: width * 0.05,
                        }}
                    >
                        {this.state.id}
                    </Text>
                    {/* COSTO */}
                    <View
                        style={{
                            flexDirection: 'row',
                            borderWidth: 0,
                            marginTop: height * 0.025,
                        }}
                    >
                        <Text
                            style={{
                                borderWidth: 0,
                                fontSize: width * 0.05
                            }}
                        >
                            {'Costo'}
                        </Text>
                        <TextInput
                            value={this.state.costo.replace('€', '')}
                            style={{
                                width: width * 0.15,
                                borderWidth: 1,
                                textAlign: 'center',
                                fontSize: width * 0.05,
                                marginLeft: width * 0.05
                            }}
                            onChangeText={(text) => {
                                this.setState({ costo: text })
                            }}
                            selectTextOnFocus
                            returnKeyLabel='Done'
                            returnKeyType='done'
                            keyboardType='number-pad'
                        />
                        <Text
                            style={{
                                borderWidth: 0,
                                fontSize: width * 0.05
                            }}
                        >
                            {'€'}
                        </Text>
                    </View>
                    {/* DURATA */}
                    <View
                        style={{
                            flexDirection: 'row',
                            borderWidth: 0,
                            marginTop: height * 0.025,
                        }}
                    >
                        <Text
                            style={{
                                borderWidth: 0,
                                fontSize: width * 0.05
                            }}
                        >
                            {'Durata'}
                        </Text>
                        <TextInput
                            value={this.state.durata.toString()}
                            style={{
                                width: width * 0.15,
                                borderWidth: 1,
                                textAlign: 'center',
                                fontSize: width * 0.05,
                                marginLeft: width * 0.05
                            }}
                            onChangeText={(text) => {
                                this.setState({ durata: text })
                            }}
                            selectTextOnFocus
                            returnKeyLabel='Done'
                            returnKeyType='done'
                            keyboardType='number-pad'
                        />
                        <Text
                            style={{
                                borderWidth: 0,
                                fontSize: width * 0.05
                            }}
                        >
                            {' minuti'}
                        </Text>
                    </View>

                    <TouchableOpacity
                        style={{
                            borderWidth: 1,
                            width: width * 0.50,
                            height: height * 0.075,
                            borderRadius: 100,
                            backgroundColor: 'black',
                            marginRight: 0,
                            marginLeft: 'auto',
                            justifyContent: 'center',
                            marginTop: width * 0.2,
                        }}
                        onPress={this.modificaOrario}
                    >
                        <Text style={{ color: 'white', fontSize: width * 0.05, fontWeight: 'bold', textAlign: 'center' }}>
                            {'CONFERMA'}
                        </Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        );
    }
}
