import React from 'react';
import { Dimensions, StyleSheet, Text, View, StatusBar, Platform, AsyncStorage, SafeAreaView, Image, Linking, ScrollView, TouchableWithoutFeedback, TouchableOpacity, BackHandler } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';

export default class AdminScreen extends React.Component {

    constructor() {
        super();

        this.state = {

        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        // works best when the goBack is async

        /*this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });*/
        Alert.alert(
            'Uscire dall\'app?',
            'Vuoi uscire dall\'app?',
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Sì', onPress: () => { console.log('OK Pressed'), BackHandler.exitApp(); } },
            ],
            { cancelable: false }
        );
        return true;
    }

    logOut = async () => {
        console.log('Effettuo logout');

        var user = firebaseAppInitialized.auth().currentUser;

        await AsyncStorage.multiSet([['facebookToken', ''], ['rimaniConnesso', 'false'], ['email', ''], ['password', ''], ['admin', 'false']], (errors) => { })
            .then(() => {
                firebaseAppInitialized.auth().app.firestore().collection('Utenti')
                    .doc(user.email).update({
                        ExponentPushToken: '',
                    }).then(() => {
                        firebaseAppInitialized.auth().signOut().then(() => {
                            this.props.navigation.navigate("Login");
                        })
                    });

            })
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View
                style={{
                    height: "90%",
                    paddingLeft: width * 0.03,
                    paddingRight: width * 0.03,
                }}
            >

                {/* TITOLO */}
                <View
                    style={{
                        flexDirection: 'row'
                    }}
                >
                    <View>
                        <Text
                            style={{
                                fontSize: width * 0.1,
                                marginTop: height * 0.05,
                                fontWeight: 'bold',
                            }}
                        >
                            {"Admin"}
                        </Text>
                    </View>

                    {/* BOTTONE LOGOUT */}
                    <View
                        style={{
                            marginRight: 0,
                            marginLeft: 'auto',
                            borderWidth: 0,
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                marginLeft: 'auto',
                                marginRight: 0,
                                marginTop: height * 0.05,
                                width: width * 0.2,
                                alignContent: 'flex-end',
                                alignItems: 'flex-end'
                            }}
                            onPress={() => {
                                this.logOut();
                            }}
                        >
                            <AntDesign
                                name={"logout"}
                                color="#007AFF"
                                size={30}
                            />
                        </TouchableOpacity>
                    </View>
                </View>

                {/* BARRA HR */}
                <View
                    style={{
                        marginTop: height * 0.02,
                        borderBottomColor: '#BCBBC1',
                        borderBottomWidth: 1
                    }}
                />

                {/* SOTTOTITOLO */}
                <Text
                    style={{
                        fontSize: width * 0.055,
                        marginTop: height * 0.04,
                    }}
                >
                    {"Scegli cosa voler fare"}
                </Text>

                {/* BOTTONE PER VISULIZZARE LE PRENOTAZIONI  */}
                <TouchableOpacity
                    style={{
                        borderWidth: 0,
                        marginTop: height * 0.075,
                    }}
                    onPress={() => {
                        this.props.navigation.navigate('AdminPrenotazioni');
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            height: height * 0.075,
                            backgroundColor: 'white',
                            borderRadius: 50,
                            borderWidth: 0.75,
                            borderColor: 'black',
                            alignContent: 'center',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Text
                            style={{
                                fontSize: width * 0.055,
                            }}
                        >
                            {'Gestisci prenotazioni'}
                        </Text>
                    </View>
                </TouchableOpacity>

                {/* BOTTONE PER AGGIUNGERE UN PRODOTTO  */}
                <TouchableOpacity
                    style={{
                        borderWidth: 0,
                        marginTop: height * 0.05,
                    }}
                    onPress={() => {
                        this.props.navigation.navigate('AdminProdotti');
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            height: height * 0.075,
                            backgroundColor: 'white',
                            borderRadius: 50,
                            borderWidth: 0.75,
                            borderColor: 'black',
                            alignContent: 'center',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Text
                            style={{
                                fontSize: width * 0.055,
                            }}
                        >
                            {'Gestisci prodotti'}
                        </Text>
                    </View>
                </TouchableOpacity>

                {/* BOTTONE PER GESTIRE GLI ORARI  */}
                <TouchableOpacity
                    style={{
                        borderWidth: 0,
                        marginTop: height * 0.05,
                    }}
                    onPress={() => {
                        this.props.navigation.navigate('AdminOrari');
                    }}
                >
                    <View
                        style={{
                            width: '100%',
                            height: height * 0.075,
                            backgroundColor: 'white',
                            borderRadius: 50,
                            borderWidth: 0.75,
                            borderColor: 'black',
                            alignContent: 'center',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                    >
                        <Text
                            style={{
                                color: 'black',
                                fontSize: width * 0.055,
                            }}
                        >
                            {'Gestisci orari'}
                        </Text>
                    </View>
                </TouchableOpacity>
            </View >
        );
    }
}
