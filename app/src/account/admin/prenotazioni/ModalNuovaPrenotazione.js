import React from 'react';
import { Dimensions, StyleSheet, Text, Modal, View, StatusBar, Platform, AsyncStorage, SafeAreaView, Image, TextInput, Linking, ScrollView, BackHandler, Alert, TouchableHighlight, TouchableOpacity, Keyboard } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons, Ionicons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Button } from 'react-native-elements'
import ModalTipoTaglio from '../../../prenota taglio/modalTipoTaglio';
import tipiTaglioManuel from '../../../prenota taglio/tipiTaglioManuel';
import tipiTaglioMarco from '../../../prenota taglio/tipiTaglioMarco';

export default class ModalNuovaPrenotazione extends React.Component {

    constructor() {
        super();

        this.state = {
            nuovaData: '2020-04-30',
            nuovoOrarioInizio: '15:30',
            nuovoOrarioFine: '16:00',
            nuovaDataGiorno: '',
            nuovaDataMese: '',
            nuovaDataAnno: '',
            nuovoOrarioInizioOra: '',
            nuovoOrarioInizioMinuto: '',
            nuovoOrarioFineOra: '',
            nuovoOrarioFineMinuto: '',
            nomeCliente: '',
            numeroCliente: '+39',
            taglioSelezionato: '',
            modelSelezioneTipoTaglioVisibility: false,
            tipiTaglio: tipiTaglioMarco,
        };
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

        /* Controllo barbiere loggato per i tipi di taglio

            var user = firebaseAppInitialized.auth().currentUser;
            user.email === 'mailDiMarco@gmail.com' ? this.setState({tipiTaglio: tipiTaglioMarco}) : this.setState({tipiTaglio: tipiTaglioManuel});
                
        */
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        // works best when the goBack is async

        /*this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });*/
        /*Alert.alert(
            'Uscire dall\'app?',
            'Vuoi uscire dall\'app?',
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Sì', onPress: () => { console.log('OK Pressed'), BackHandler.exitApp(); } },
            ],
            { cancelable: false }
        );
        return true;*/
    }

    selectTaglio = (tipoTaglio, durata) => {

        this.setState({
            taglioSelezionato: tipoTaglio,
            durataTaglio: durata,
            modelSelezioneTipoTaglioVisibility: false,
        })

        console.log('Selezionato : ' + tipoTaglio);
    }

    aggiungiPrenotazione = () => {
        if (this.state.nuovaDataGiorno !== '' && this.state.nuovaDataMese !== '' && this.state.nuovaDataAnno !== '' &&
            this.state.nuovoOrarioInizioOra !== '' && this.state.nuovoOrarioInizioMinuto !== '' &&
            this.state.nuovoOrarioFineOra !== '' && this.state.nuovoOrarioFineMinuto !== '') {
            console.log('Modifico prenotazione ' + this.props.id);

            var nuovaData = this.state.nuovaDataAnno + '-' + this.state.nuovaDataMese + '-' + this.state.nuovaDataGiorno;
            var nuovoOrarioInizio = this.state.nuovoOrarioInizioOra + ':' + this.state.nuovoOrarioInizioMinuto;
            var nuovoOrarioFine = this.state.nuovoOrarioFineOra + ':' + this.state.nuovoOrarioFineMinuto;
            var nomeCliente = this.state.nomeCliente;
            var numeroCliente = this.state.numeroCliente;
            var tipoTaglio = this.state.taglioSelezionato;

            var barbiere = 'Marco';

            /* Controllo barbiere loggato per aggiungere la prenotazione

            var user = firebaseAppInitialized.auth().currentUser;
            user.email === 'mailDiMarco@gmail.com' ? barbiere = 'Marco' : barbiere= ' Manuel';
                
            */


            console.log('Nuova data : ' + nuovaData);
            console.log('Nuovo orario inizio: ' + nuovoOrarioInizio);
            console.log('Nuovo orario fine: ' + nuovoOrarioFine);

            firebaseAppInitialized.auth().app.firestore().collection('Prenotazioni')
                .doc().set({
                    data_prenotazione: nuovaData,
                    orario_inizio: nuovoOrarioInizio,
                    orario_fine: nuovoOrarioFine,
                    cellulare: numeroCliente,
                    barbiere: barbiere,
                    tipo_taglio: tipoTaglio,
                    effettuata: false,
                    eliminata: false,
                    cliente: nomeCliente,
                }).then(() => {
                    console.log('Prenotazione aggiunta');

                    this.props.hide(true);
                })
        }
    }

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <Modal
                animationType="slide" // Default- slide
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => {
                    this.props.hide(false);
                }}>
                <View style={{ marginTop: Platform.OS === 'ios' ? Constants.statusBarHeight : 0, flex: 1, backgroundColor: 'white', padding: width * 0.03, }}>
                    {/* -------- MODAL SELEZIONE TIPO TAGLIO -------------- */}
                    <ModalTipoTaglio
                        modelSelezioneTipoTaglioVisibility={this.state.modelSelezioneTipoTaglioVisibility}
                        closeModal={this.closeModal}
                        selectTaglio={this.selectTaglio}
                        tipiTaglio={this.state.tipiTaglio}
                    />
                    {/* -------- FINE MODAL SELEZIONE TIPO TAGLIO -------------- */}
                    <KeyboardAwareScrollView>
                        {/* BOTTONE INDIETRO */}
                        <TouchableOpacity
                            style={{
                                width: width * 0.3,
                            }}
                            onPress={() => {
                                this.props.hide(false);
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderWidth: 0,
                                }}
                            >
                                <Ionicons
                                    name='ios-arrow-back'
                                    size={width * 0.1}
                                    color={'#007AFF'}
                                />
                                <Text
                                    style={{
                                        borderWidth: 0,
                                        alignSelf: 'center',
                                        color: '#007AFF',
                                        fontSize: width * 0.05,
                                        marginLeft: width * 0.03,
                                        fontWeight: '100'
                                    }}
                                >
                                    {'Indietro'}
                                </Text>
                            </View>
                        </TouchableOpacity>
                        {/* TITOLO */}
                        <Text
                            style={{
                                fontSize: width * 0.075,
                                marginTop: height * 0.02,
                                fontWeight: 'bold',
                            }}
                        >
                            {"Aggiungi prenotazione"}
                        </Text>

                        <View
                            style={{
                                marginTop: height * 0.02,
                                borderBottomColor: '#BCBBC1',
                                borderBottomWidth: 1
                            }}
                        />
                        {/* MODIFICA DATA */}
                        <Text
                            style={{
                                marginTop: height * 0.02,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Data prenotazione'}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                                marginTop: height * 0.01,
                            }}
                        >
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovaDataGiorno: text })
                                    if (text.length > 1) {
                                        this.mese.focus();
                                    }
                                }}
                                onSubmitEditing={() => {
                                    this.mese.focus();
                                }}
                                selectTextOnFocus
                                keyboardType='number-pad'
                                placeholder='GG'
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                ref={ref => this.giorno = ref}
                            />
                            <View
                                style={{
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    justifyContent: 'center'
                                }}
                            >
                                <Text style={{ fontSize: width * 0.05, }}>{'/'}</Text>
                            </View>
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovaDataMese: text })
                                    if (text.length > 1) {
                                        this.anno.focus();
                                    }
                                }}
                                onSubmitEditing={() => {
                                    this.anno.focus();
                                }}
                                keyboardType='number-pad'
                                selectTextOnFocus
                                placeholder='MM'
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                ref={ref => this.mese = ref}
                            />
                            <View
                                style={{
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    justifyContent: 'center'
                                }}
                            >
                                <Text style={{ fontSize: width * 0.05, }}>{'/'}</Text>
                            </View>
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovaDataAnno: text })
                                    if (text.length > 3)
                                        this.inizioOra.focus()
                                }}
                                onSubmitEditing={() => {
                                    this.inizioOra.focus();
                                }}
                                keyboardType='number-pad'
                                selectTextOnFocus
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                placeholder='YYYY'
                                ref={ref => this.anno = ref}
                            />
                        </View>
                        {/* MODIFICA ORARIO INIZIO*/}
                        <Text
                            style={{
                                marginTop: height * 0.01,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Orario inizio taglio'}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                                marginTop: height * 0.01,
                            }}
                        >
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovoOrarioInizioOra: text })
                                    if (text.length > 1)
                                        this.inizioMinuto.focus()
                                }}
                                onSubmitEditing={() => {
                                    this.inizioMinuto.focus();
                                }}
                                selectTextOnFocus
                                keyboardType='number-pad'
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                placeholder='HH'
                                ref={ref => this.inizioOra = ref}
                            />
                            <View
                                style={{
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    justifyContent: 'center'
                                }}
                            >
                                <Text style={{ fontSize: width * 0.05, }}>{':'}</Text>
                            </View>
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovoOrarioInizioMinuto: text })
                                    if (text.length > 1)
                                        this.fineOra.focus();
                                }}
                                onSubmitEditing={() => {
                                    this.fineOra.focus();
                                }}
                                keyboardType='number-pad'
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                placeholder='MM'
                                selectTextOnFocus
                                ref={ref => this.inizioMinuto = ref}
                            />
                        </View>
                        {/* MODIFICA ORARIO FINE*/}
                        <Text
                            style={{
                                marginTop: height * 0.01,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Orario fine taglio'}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                                marginTop: height * 0.01,
                            }}
                        >
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovoOrarioFineOra: text })
                                    if (text.length > 1)
                                        this.fineMinuto.focus();
                                }}
                                onSubmitEditing={() => {
                                    this.fineMinuto.focus();
                                }}
                                keyboardType='number-pad'
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                placeholder='HH'
                                selectTextOnFocus
                                ref={ref => this.fineOra = ref}
                            />
                            <View
                                style={{
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    justifyContent: 'center'
                                }}
                            >
                                <Text style={{ fontSize: width * 0.05, }}>{':'}</Text>
                            </View>
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovoOrarioFineMinuto: text })
                                    if (text.length > 1)
                                        this.nomeCliente.focus();
                                }}
                                onSubmitEditing={() => {
                                    this.nomeCliente.focus();
                                }}
                                keyboardType='number-pad'
                                returnKeyLabel='Done'
                                returnKeyType='done'

                                placeholder='MM'
                                selectTextOnFocus
                                ref={ref => this.fineMinuto = ref}
                            />
                        </View>
                        {/* NOME CLIENTE*/}
                        <Text
                            style={{
                                marginTop: height * 0.01,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Nome cliente'}
                        </Text>
                        <TextInput
                            style={{
                                width: width,
                                height: height * 0.05,
                                borderWidth: 0,
                                fontSize: width * 0.05,
                                textAlign: 'left',
                                marginTop: height * 0.01,
                            }}
                            onChangeText={(text) => {
                                this.setState({ nomeCliente: text })
                            }}
                            onSubmitEditing={() => {
                                this.numeroCliente.focus();
                            }}
                            keyboardType='default'
                            returnKeyLabel='Done'
                            returnKeyType='done'

                            placeholder='Nome'
                            selectTextOnFocus
                            ref={ref => this.nomeCliente = ref}
                        />
                        {/* NUMERO CLIENTE*/}
                        <Text
                            style={{
                                marginTop: height * 0.01,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Numero di telefono del cliente'}
                        </Text>
                        <TextInput
                            style={{
                                width: width,
                                height: height * 0.05,
                                borderWidth: 0,
                                fontSize: width * 0.05,
                                textAlign: 'left',
                                marginTop: height * 0.01,
                            }}
                            onChangeText={(text) => {
                                this.setState({ numeroCliente: text })
                            }}
                            onSubmitEditing={() => {
                                Keyboard.dismiss();
                            }}
                            keyboardType='phone-pad'
                            returnKeyLabel='Done'
                            returnKeyType='done'

                            placeholder='Numero'
                            ref={ref => this.numeroCliente = ref}
                            value={this.state.numeroCliente}
                        />
                        {/* ----------- SELEZIONE TIPO TAGLIO ---------------*/}
                        <Text style={{
                            marginTop: height * 0.01,
                            fontSize: width * 0.05,
                        }}>
                            {"Scegli il tipo di taglio"}
                        </Text>

                        <Button //Tasto selezione data per il taglio

                            buttonStyle={{
                                backgroundColor: 'white',
                                borderBottomWidth: 1,
                                borderColor: 'black',
                                width: '100%',
                                marginLeft: 0,
                                marginRight: 'auto',
                                marginTop: height * 0.025,
                                justifyContent: 'flex-start',
                            }}
                            containerStyle={{
                            }}
                            titleStyle={{
                                color: 'black',
                                textAlign: 'left',
                                fontWeight: '100',
                                fontSize: height * 0.02,
                                marginLeft: 10
                            }}

                            onPress={() => {
                                if (this.state.selected !== '0') {
                                    this.setState({
                                        modelSelezioneTipoTaglioVisibility: true
                                    })
                                }
                            }}

                            icon={
                                <EvilIcons
                                    name="calendar"
                                    size={height * 0.03}
                                    color="black"
                                />
                            }
                            title={this.state.taglioSelezionato === '' ? "Seleziona taglio" : this.state.taglioSelezionato}
                        />


                        {/* ----------- FINE SELEZIONE TIPO TAGLIO ---------------*/}
                        {/* TASTO AGGIUNGI*/}
                        <TouchableOpacity
                            style={{
                                borderWidth: 1,
                                width: width * 0.50,
                                height: height * 0.075,
                                borderRadius: 100,
                                backgroundColor: 'black',
                                marginRight: 0,
                                marginLeft: 'auto',
                                justifyContent: 'center',
                                marginTop: width * 0.2,
                            }}
                            onPress={this.aggiungiPrenotazione}
                        >
                            <Text style={{ color: 'white', fontSize: width * 0.05, fontWeight: 'bold', textAlign: 'center' }}>
                                {'AGGIUNGI'}
                            </Text>
                        </TouchableOpacity>
                    </KeyboardAwareScrollView>
                </View>
            </Modal>
        );
    }
}
