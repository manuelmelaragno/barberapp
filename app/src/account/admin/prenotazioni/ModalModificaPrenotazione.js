import React from 'react';
import { Dimensions, StyleSheet, Text, Modal, View, StatusBar, Platform, AsyncStorage, SafeAreaView, Image, TextInput, Linking, ScrollView, BackHandler, Alert, TouchableHighlight, TouchableOpacity, Keyboard } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons, Ionicons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class ModalModificaPrenotazione extends React.Component {

    constructor() {
        super();

        this.state = {
            nuovaData: '2020-04-30',
            nuovoOrarioInizio: '15:30',
            nuovoOrarioFine: '16:00',
            nuovaDataGiorno: '',
            nuovaDataMese: '',
            nuovaDataAnno: '',
            nuovoOrarioInizioOra: '',
            nuovoOrarioInizioMinuto: '',
            nuovoOrarioFineOra: '',
            nuovoOrarioFineMinuto: '',
        };
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        // works best when the goBack is async

        /*this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });*/
        /*Alert.alert(
            'Uscire dall\'app?',
            'Vuoi uscire dall\'app?',
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Sì', onPress: () => { console.log('OK Pressed'), BackHandler.exitApp(); } },
            ],
            { cancelable: false }
        );
        return true;*/
    }

    modificaPrenotazione = async () => {
        if (this.state.nuovaDataGiorno !== '' && this.state.nuovaDataMese !== '' && this.state.nuovaDataAnno !== '' &&
            this.state.nuovoOrarioInizioOra !== '' && this.state.nuovoOrarioInizioMinuto !== '' &&
            this.state.nuovoOrarioFineOra !== '' && this.state.nuovoOrarioFineMinuto !== '') {
            console.log('Modifico prenotazione ' + this.props.id);

            var nuovaData = this.state.nuovaDataAnno + '-' + this.state.nuovaDataMese + '-' + this.state.nuovaDataGiorno;
            var nuovoOrarioInizio = this.state.nuovoOrarioInizioOra + ':' + this.state.nuovoOrarioInizioMinuto;
            var nuovoOrarioFine = this.state.nuovoOrarioFineOra + ':' + this.state.nuovoOrarioFineMinuto;
            console.log('Nuova data : ' + nuovaData);
            console.log('Nuovo orario inizio: ' + nuovoOrarioInizio);
            console.log('Nuovo orario fine: ' + nuovoOrarioFine);

            firebaseAppInitialized.auth().app.firestore().collection('Prenotazioni')
                .doc(this.props.id).update({
                    data_prenotazione: nuovaData,
                    orario_inizio: nuovoOrarioInizio,
                    orario_fine: nuovoOrarioFine,
                }).then(async () => {
                    console.log('Prenotazione modificata');

                    /// INVIO NOTIFICA ALL'UTENTE
                    //PRENDO IL TOKEN DA FIREBASE
                    await firebaseAppInitialized.auth().app.firestore().collection('Utenti')
                        .where('email', '==', this.props.email)
                        .get()
                        .then(async (querySnapshot) => {
                            querySnapshot.forEach(async (doc) => {
                                // doc.data() is never undefined for query doc snapshots
                                console.log(doc.id, " => ", doc.data());
                                var ExponentPushToken = doc.data().ExponentPushToken;
                                var dataTaglio = nuovaData;

                                //INVIO NOTIFICA
                                const message = {
                                    to: ExponentPushToken,
                                    sound: 'default',
                                    title: 'Prenotazione Modificata!',
                                    body: 'Prenotazione modificata\nNuova prenotazione ' + dataTaglio + ' ' + nuovoOrarioInizio + '-' + nuovoOrarioFine,
                                    data: { data: 'goes here' },
                                    _displayInForeground: true,
                                };
                                const response = await fetch('https://exp.host/--/api/v2/push/send', {
                                    method: 'POST',
                                    headers: {
                                        Accept: 'application/json',
                                        'Accept-encoding': 'gzip, deflate',
                                        'Content-Type': 'application/json',
                                    },
                                    body: JSON.stringify(message),
                                }).then(() => {
                                    console.log('Notifica inviata');
                                }).catch((errors) => {
                                    console.log('Errore notifica');
                                    console.log(errors);

                                });
                            });
                        })
                        .catch(function (error) {
                            console.log("Error getting documents: ", error);
                        });
                    ///
                    console.log('Prenotazione modificata');
                    this.props.hide(true);
                })
        }
    }

    selectNextInput = (ref) => {

    }

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => {
                    this.props.hide(false);
                }}>
                <View style={{ marginTop: Platform.OS === 'ios' ? Constants.statusBarHeight : 0, flex: 1, backgroundColor: 'white', padding: width * 0.03, }}>
                    <KeyboardAwareScrollView>
                        {/* BOTTONE INDIETRO */}
                        <TouchableOpacity
                            style={{
                                width: width * 0.3,
                            }}
                            onPress={() => {
                                this.props.hide(false);
                            }}
                        >
                            <View
                                style={{
                                    flexDirection: 'row',
                                    borderWidth: 0,
                                }}
                            >
                                <Ionicons
                                    name='ios-arrow-back'
                                    size={width * 0.1}
                                    color={'#007AFF'}
                                />
                                <Text
                                    style={{
                                        borderWidth: 0,
                                        alignSelf: 'center',
                                        color: '#007AFF',
                                        fontSize: width * 0.05,
                                        marginLeft: width * 0.03,
                                        fontWeight: '100'
                                    }}
                                >
                                    {'Indietro'}
                                </Text>
                            </View>
                        </TouchableOpacity>
                        {/* TITOLO */}
                        <Text
                            style={{
                                fontSize: width * 0.075,
                                marginTop: height * 0.02,
                                fontWeight: 'bold',
                            }}
                        >
                            {"Modifica prenotazione"}
                        </Text>

                        <View
                            style={{
                                marginTop: height * 0.02,
                                borderBottomColor: '#BCBBC1',
                                borderBottomWidth: 1
                            }}
                        />
                        {/* MODIFICA DATA */}
                        <Text
                            style={{
                                marginTop: height * 0.04,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Modifica data'}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                                marginTop: height * 0.025,
                            }}
                        >
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovaDataGiorno: text })
                                    if (text.length > 1) {
                                        this.mese.focus();
                                    }
                                }}
                                onSubmitEditing={() => {
                                    this.mese.focus();
                                }}
                                selectTextOnFocus
                                keyboardType='number-pad'
                                placeholder='GG'
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                ref={ref => this.giorno = ref}
                            />
                            <View
                                style={{
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    justifyContent: 'center'
                                }}
                            >
                                <Text style={{ fontSize: width * 0.05, }}>{'/'}</Text>
                            </View>
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovaDataMese: text })
                                    if (text.length > 1) {
                                        this.anno.focus();
                                    }
                                }}
                                onSubmitEditing={() => {
                                    this.anno.focus();
                                }}
                                keyboardType='number-pad'
                                selectTextOnFocus
                                placeholder='MM'
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                ref={ref => this.mese = ref}
                            />
                            <View
                                style={{
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    justifyContent: 'center'
                                }}
                            >
                                <Text style={{ fontSize: width * 0.05, }}>{'/'}</Text>
                            </View>
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovaDataAnno: text })
                                    if (text.length > 3)
                                        this.inizioOra.focus()
                                }}
                                onSubmitEditing={() => {
                                    this.inizioOra.focus();
                                }}
                                keyboardType='number-pad'
                                selectTextOnFocus
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                placeholder='YYYY'
                                ref={ref => this.anno = ref}
                            />
                        </View>
                        {/* MODIFICA ORARIO INIZIO*/}
                        <Text
                            style={{
                                marginTop: height * 0.04,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Modifica orario inizio taglio'}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                                marginTop: height * 0.025,
                            }}
                        >
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovoOrarioInizioOra: text })
                                    if (text.length > 1)
                                        this.inizioMinuto.focus()
                                }}
                                onSubmitEditing={() => {
                                    this.inizioMinuto.focus();
                                }}
                                selectTextOnFocus
                                keyboardType='number-pad'
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                placeholder='HH'
                                ref={ref => this.inizioOra = ref}
                            />
                            <View
                                style={{
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    justifyContent: 'center'
                                }}
                            >
                                <Text style={{ fontSize: width * 0.05, }}>{':'}</Text>
                            </View>
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovoOrarioInizioMinuto: text })
                                    if (text.length > 1)
                                        this.fineOra.focus();
                                }}
                                onSubmitEditing={() => {
                                    this.fineOra.focus();
                                }}
                                keyboardType='number-pad'
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                placeholder='MM'
                                selectTextOnFocus
                                ref={ref => this.inizioMinuto = ref}
                            />
                        </View>
                        {/* MODIFICA ORARIO FINE*/}
                        <Text
                            style={{
                                marginTop: height * 0.04,
                                fontSize: width * 0.05,
                            }}
                        >
                            {'Modifica orario fine taglio'}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                                marginTop: height * 0.025,
                            }}
                        >
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovoOrarioFineOra: text })
                                    if (text.length > 1)
                                        this.fineMinuto.focus();
                                }}
                                onSubmitEditing={() => {
                                    this.fineMinuto.focus();
                                }}
                                keyboardType='number-pad'
                                returnKeyLabel='Done'
                                returnKeyType='done'
                                placeholder='HH'
                                selectTextOnFocus
                                ref={ref => this.fineOra = ref}
                            />
                            <View
                                style={{
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    justifyContent: 'center'
                                }}
                            >
                                <Text style={{ fontSize: width * 0.05, }}>{':'}</Text>
                            </View>
                            <TextInput
                                style={{
                                    width: width * 0.15,
                                    height: height * 0.05,
                                    borderWidth: 0,
                                    fontSize: width * 0.05,
                                    textAlign: 'center',
                                }}
                                onChangeText={(text) => {
                                    this.setState({ nuovoOrarioFineMinuto: text })
                                    if (text.length > 1)
                                        Keyboard.dismiss();
                                }}
                                onSubmitEditing={() => {
                                    Keyboard.dismiss();
                                }}
                                keyboardType='number-pad'
                                returnKeyLabel='Done'
                                returnKeyType='done'

                                placeholder='MM'
                                selectTextOnFocus
                                ref={ref => this.fineMinuto = ref}
                            />
                        </View>
                        <TouchableOpacity
                            style={{
                                borderWidth: 1,
                                width: width * 0.50,
                                height: height * 0.075,
                                borderRadius: 100,
                                backgroundColor: 'black',
                                marginRight: 0,
                                marginLeft: 'auto',
                                justifyContent: 'center',
                                marginTop: width * 0.2,
                            }}
                            onPress={this.modificaPrenotazione}
                        >
                            <Text style={{ color: 'white', fontSize: width * 0.05, fontWeight: 'bold', textAlign: 'center' }}>
                                {'MODIFICA'}
                            </Text>
                        </TouchableOpacity>
                    </KeyboardAwareScrollView>
                </View>
            </Modal>
        );
    }
}
