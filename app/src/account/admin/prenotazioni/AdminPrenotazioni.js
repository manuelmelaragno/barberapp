import React from 'react';
import { Dimensions, ActivityIndicator, Text, View, FlatList, Platform, Linking, BackHandler, TouchableOpacity, Alert, RefreshControl } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons, Ionicons, MaterialIcons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';
import ModalModificaPrenotazione from './ModalModificaPrenotazione'
import ModalNuovaPrenotazione from './ModalNuovaPrenotazione'

export default class AdminPrenotazioni extends React.Component {

    constructor() {
        super();

        this.state = {
            limitQuery: 15,
            pivot: null,
            prenotazioni: [],
            loadingPrentoazioni: true,
            modalModificaPrenotazioneVisibility: false, // Default - false
            idPrenotazioneSelezionata: '',
            modalNuovaPrenotazioneVisibility: false, // Default - false
            emailSelezionata: '',
        };
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

        this.caricaPrenotazioni();
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    caricaPrenotazioni = () => {
        //CARICO LE PRENOTAZIONI
        console.log('Carico prenotazioni...');

        var pivot = this.state.pivot;
        var limit = this.state.limitQuery;
        var user = firebaseAppInitialized.auth().currentUser;

        // DA AGGIUNGERE FILTRO PER BARBIERE

        ////

        firebaseAppInitialized.auth().app.firestore().collection('Prenotazioni')
            .where('effettuata', '==', false).where('eliminata', '==', false).orderBy('data_prenotazione').startAfter(pivot).limit(limit)
            .get()
            .then((querySnapshot) => {
                if (querySnapshot.docs.length > 0) {
                    pivot = querySnapshot.docs[querySnapshot.docs.length - 1];
                    //console.log(pivot);
                    querySnapshot.forEach((doc) => {
                        // doc.data() is never undefined for query doc snapshots
                        console.log(doc.id, " => ", doc.data());
                        var dataPrenotazione = doc.data().data_prenotazione;
                        dataPrenotazione = dataPrenotazione.split('-')[2] + '/' + dataPrenotazione.split('-')[1] + '/' + dataPrenotazione.split('-')[0];
                        var prenotazioni = this.state.prenotazioni;
                        prenotazioni.push({
                            cliente: doc.data().cliente,
                            dataPrenotazione: dataPrenotazione,
                            orario: doc.data().orario_inizio + ' - ' + doc.data().orario_fine,
                            tipoTaglio: doc.data().tipo_taglio,
                            cellulareCliente: doc.data().cellulare,
                            emailCliente: doc.data().email !== null && doc.data().email !== null ? doc.data().email : '',
                            idPrenotazione: doc.id,
                            email: doc.data().email
                        })
                        this.setState({
                            prenotazioni: prenotazioni,
                            loadingPrentoazioni: false,
                        })
                    });

                    this.setState({
                        limitQuery: limit,
                        pivot: pivot,
                    })
                }
            })
    }

    handleBackPress = () => {
        // works best when the goBack is async

        /*this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });*/
        Alert.alert(
            'Uscire dall\'app?',
            'Vuoi uscire dall\'app?',
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Sì', onPress: () => { console.log('OK Pressed'), BackHandler.exitApp(); } },
            ],
            { cancelable: false }
        );
        return true;
    }

    prenotazioneFatta = (idPrenotazione) => {
        console.log('Prenotazione ' + idPrenotazione + ' soddisfatta');
        Alert.alert(
            'Cliente servito',
            'Il cliente è stato servito?',
            [
                {
                    text: 'No',
                    onPress: () => {
                        console.log('Prenotazione ' + idPrenotazione + ' NON effettuata...');
                    },
                    style: 'cancel',
                },
                {
                    text: 'Sì', onPress: () => {
                        // AGGIORNO DB PER L'ELIMINAZIONE DELLA PRENOTAZIONE
                        firebaseAppInitialized.auth().app.firestore().collection('Prenotazioni')
                            .doc(idPrenotazione).update({
                                effettuata: true,
                            }).then(() => {
                                console.log('Prenotazione ' + idPrenotazione + ' effettuata...');
                                this.setState({
                                    pivot: null,
                                    loadingPrentoazioni: true,
                                }, () => {
                                    this.caricaPrenotazioni()
                                })
                            });
                    },
                }
            ],
            { cancelable: false }
        );
    }

    eliminaPrenotazione = (idPrenotazione) => {
        console.log('Prenotazione ' + idPrenotazione + ' in eliminazione...');
        Alert.alert(
            'Elimina prenotazione',
            'Vuoi eliminare la prenotazione?',
            [
                {
                    text: 'No',
                    onPress: () => {
                        console.log('Prenotazione ' + idPrenotazione + ' NON eliminata...');
                    },
                    style: 'cancel',
                },
                {
                    text: 'Sì', onPress: () => {
                        // AGGIORNO DB PER L'ELIMINAZIONE DELLA PRENOTAZIONE
                        firebaseAppInitialized.auth().app.firestore().collection('Prenotazioni')
                            .doc(idPrenotazione).update({
                                eliminata: true,
                            }).then(() => {
                                console.log('Prenotazione ' + idPrenotazione + ' eliminata...');
                                this.setState({
                                    pivot: null,
                                    loadingPrentoazioni: true,
                                }, () => {
                                    this.caricaPrenotazioni()
                                })
                            });
                    },
                }
            ],
            { cancelable: false }
        );
    }

    showModalModificaPrenotazione = (id, email) => {
        console.log(email);

        this.setState({
            modalModificaPrenotazioneVisibility: true,
            idPrenotazioneSelezionata: id,
            emailSelezionata: email,
        }, () => console.log(this.state.modalModificaPrenotazioneVisibility))
    }

    hideModalModificaPrenotazione = (aggiorna) => {
        console.log('Hide modal');

        this.setState({
            modalModificaPrenotazioneVisibility: false,
            modalNuovaPrenotazioneVisibility: false,
        }, () => console.log(this.state.modalModificaPrenotazioneVisibility))

        if (aggiorna) {
            this.setState({
                pivot: null,
                loadingPrentoazioni: true,
                prenotazioni: [],
            }, () => {
                this.caricaPrenotazioni();
                this.refs.toast.show('  Prenotazione modificata con successo  ', 750);
            })
        }
    }

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <View
                style={{
                    height: "90%",
                    paddingLeft: width * 0.03,
                    paddingRight: width * 0.03,
                    paddingTop: width * 0.03,
                }}
            >
                <ModalModificaPrenotazione visible={this.state.modalModificaPrenotazioneVisibility} hide={this.hideModalModificaPrenotazione} id={this.state.idPrenotazioneSelezionata} email={this.state.emailSelezionata} />
                <ModalNuovaPrenotazione visible={this.state.modalNuovaPrenotazioneVisibility} hide={this.hideModalModificaPrenotazione} />
                <View
                    style={{
                        flexDirection: 'row',
                    }}
                >
                    {/* BOTTONE INDIETRO */}
                    <TouchableOpacity
                        style={{
                            width: width * 0.3,
                        }}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                    >
                        <View
                            style={{
                                flexDirection: 'row',
                                borderWidth: 0,
                            }}
                        >
                            <Ionicons
                                name='ios-arrow-back'
                                size={width * 0.1}
                                color={'#007AFF'}
                            />
                            <Text
                                style={{
                                    borderWidth: 0,
                                    alignSelf: 'center',
                                    color: '#007AFF',
                                    fontSize: width * 0.05,
                                    marginLeft: width * 0.03,
                                    fontWeight: '100'
                                }}
                            >
                                {'Indietro'}
                            </Text>
                        </View>
                    </TouchableOpacity>
                    {/* BOTTONE AGGIUNGI */}
                    <TouchableOpacity
                        style={{
                            width: width * 0.15,
                            marginLeft: 'auto',
                            marginRight: 0,
                            alignContent: 'center',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}
                        onPress={() => {
                            // APRI MODAL NUOVA PRENOTAZIONE
                            this.setState({
                                modalNuovaPrenotazioneVisibility: true,
                            })
                        }}
                    >
                        <Ionicons
                            name='ios-add'
                            size={width * 0.1}
                            color={'#007AFF'}
                        />
                    </TouchableOpacity>
                </View>
                {/* TITOLO */}
                <Text
                    style={{
                        fontSize: width * 0.1,
                        marginTop: height * 0.02,
                        fontWeight: 'bold',
                    }}
                >
                    {"Prenotazioni"}
                </Text>

                <View
                    style={{
                        marginTop: height * 0.02,
                        borderBottomColor: '#BCBBC1',
                        borderBottomWidth: 1
                    }}
                />

                {/* LISTA PRENOTAZIONI */}

                <FlatList
                    data={this.state.prenotazioni}
                    renderItem={({ item }) =>
                        <Item cliente={item.cliente} emailCliente={item.email} data={item.dataPrenotazione + ' ' + item.orario} tipoTaglio={item.tipoTaglio} idPrenotazione={item.idPrenotazione} cellulare={item.cellulareCliente} prenotazioneFatta={this.prenotazioneFatta} eliminaPrenotazione={this.eliminaPrenotazione} showModalModificaPrenotazione={this.showModalModificaPrenotazione} />
                    }
                    keyExtractor={item => item.idPrenotazione}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.loadingPrentoazioni}
                            onRefresh={() => {
                                this.setState({
                                    pivot: null,
                                    loadingPrentoazioni: true,
                                    prenotazioni: [],
                                }, () => {
                                    this.caricaPrenotazioni()
                                })
                            }}
                        />
                    }
                    onEndReachedThreshold={0.3}
                    onEndReached={() => {
                        console.log('Carico altre prenotazioni...');
                        this.caricaPrenotazioni();
                    }}
                />
                <Toast
                    ref="toast"
                    fadeOutDuration={250}
                    fadeInDuration={250}
                    textStyle={{
                        fontWeight: '100',
                        color: 'white',
                        fontSize: width * 0.04,
                    }}
                    style={{
                        borderRadius: 100,
                    }}
                    positionValue={height * 0.25}
                />
            </View >
        );
    }
}

function Item({ cliente, emailCliente, data, tipoTaglio, idPrenotazione, cellulare, prenotazioneFatta, eliminaPrenotazione, showModalModificaPrenotazione }) {
    var { height, width } = Dimensions.get('window');
    return (
        <View
            style={{
                borderWidth: 0,
                width: '100%',
                height: height * 0.15,
                marginTop: height * 0.02,
                backgroundColor: '#f7f7f7',
                borderRadius: 5,
                paddingLeft: width * 0.02,
            }}
        >
            <View
                style={{
                    flexDirection: 'row'
                }}
            >
                {/* COLONNA 1 - INFO PRENOTAZIONE */}
                <View
                    style={{
                        borderWidth: 0,
                        borderColor: 'red',
                        width: '70%',
                        height: height * 0.146,
                        justifyContent: 'center'
                    }}
                >
                    <Text
                        style={{
                            fontSize: width * 0.0475,
                            fontWeight: 'bold',
                        }}
                    >
                        {cliente}
                    </Text>
                    <Text
                        style={{
                            fontSize: width * 0.0425,
                            color: '#222222'
                        }}
                    >
                        {data}
                    </Text>

                    <Text
                        style={{
                            fontSize: width * 0.0425,
                            color: '#222222'
                        }}
                    >
                        {tipoTaglio}
                    </Text>
                </View>
                {/* COLONNA 2 - AZIONI PRENOTAZIONE */}
                <View
                    style={{
                        borderWidth: 0,
                        borderColor: 'black',
                        width: '30%',
                        height: height * 0.146,
                        flexDirection: 'row'
                    }}
                >
                    <View
                        style={{
                            borderWidth: 0,
                            borderColor: 'green',
                            width: '50%',
                            height: height * 0.146,
                            justifyContent: 'center'
                        }}
                    >

                        <TouchableOpacity
                            style={{ marginLeft: 'auto', marginRight: 'auto' }}
                            onPress={() => {
                                console.log(cellulare);

                                Linking.canOpenURL('tel:' + cellulare)
                                    .then(supported => {
                                        if (supported) {
                                            Platform.OS === 'android' ? Linking.openURL('tel:' + cellulare) : Linking.openURL('tel://' + cellulare).then(promise => { }).catch(error => { });

                                        }
                                    }).catch(error => {
                                        console.log('Errori');

                                    })
                            }}
                        >
                            <Ionicons
                                name="ios-call"
                                color="black"
                                size={width * 0.12}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{ marginLeft: 'auto', marginRight: 'auto' }}
                            onPress={() => {
                                showModalModificaPrenotazione(idPrenotazione, emailCliente);
                            }}
                        >
                            <Ionicons
                                name="ios-create"
                                color="black"
                                size={width * 0.12}
                            />
                        </TouchableOpacity>
                    </View>
                    <View
                        style={{
                            borderWidth: 0,
                            borderColor: 'green',
                            width: '50%',
                            height: height * 0.146,
                            justifyContent: 'center'
                        }}
                    >
                        <TouchableOpacity
                            style={{ marginLeft: 'auto', marginRight: 'auto' }}
                            onPress={() => {
                                prenotazioneFatta(idPrenotazione);
                            }}
                        >
                            <Ionicons
                                name="ios-checkmark-circle"
                                color="green"
                                size={width * 0.12}
                            />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{ marginLeft: 'auto', marginRight: 'auto' }}
                            onPress={() => {
                                eliminaPrenotazione(idPrenotazione);
                            }}
                        >
                            <Ionicons
                                name="ios-close-circle"
                                color="red"
                                size={width * 0.12}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}
