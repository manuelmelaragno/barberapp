import React from 'react';
import { Alert, AsyncStorage, BackHandler, Dimensions, StyleSheet, Text, View, StatusBar, Platform, SafeAreaView, Image, Linking, ScrollView, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';
import { Input, Button, CheckBox } from 'react-native-elements';
import { EvilIcons, AntDesign, SimpleLineIcons, MaterialIcons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../firebase/firebase';
import * as Facebook from 'expo-facebook';
import Toast, { DURATION } from 'react-native-easy-toast';
import PushNotificationConfig from '../login/PushNotificationConfig';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';

export default class Login extends React.Component {

    constructor() {
        super();

        this.state = {
            password: '',
            email: '',
            rimaniConnesso: true,
        }
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.setState({
            password: '',
            email: '',
            rimaniConnesso: true,
        })
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        // works best when the goBack is async

        /*this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });*/
        Alert.alert(
            'Uscire dall\'app?',
            'Vuoi uscire dall\'app?',
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Sì', onPress: () => { console.log('OK Pressed'), BackHandler.exitApp(); } },
            ],
            { cancelable: false }
        );
        return true;
    }

    // EFFETTUO IL LOGIN CON FACEBOOK
    async logInWithFacebook() {
        //firebaseAppInitialized.auth().useDeviceLanguage();
        try {


            Facebook.initializeAsync('2735129603202539', 'Barber App')
                .then(() => {
                    console.log('Inizializzata');
                }).catch((error) => {
                    console.log(error);
                });

            const {
                type,
                token,
                expires,
                permissions,
                declinedPermissions,
            } = await Facebook.logInWithReadPermissionsAsync('', {
                permissions: ['public_profile', 'email'],
            });
            if (type === 'success') {
                // Ottengo le informazioni dell'utente
                /*var response = await fetch('https://graph.facebook.com/me?access_token=' + token);
                console.log('Logged in!', `Hi ${(await response.json()).name}!`);*/

                console.log('loggato, expires : ' + expires);
                console.log('Permessi negati : ' + declinedPermissions);

                // SALVO IL TOKEN IN MEMORIA, IN MODO CHE LO USO FINO ALL'EXPIRE
                try {
                    await AsyncStorage.setItem('facebookToken', token, (errors) => {
                        console.log(token);
                    }).then(() => {
                        this.props.navigation.navigate('Account');
                    })
                } catch (error) {
                    // Error saving data
                    console.log(error);
                }

            } else {
                // type === 'cancel'
                console.log("CANCELLATO : " + token);

            }
        } catch ({ message }) {
            alert('Facebook Login Error: ' + message);
        }
    }

    // LOGIN NORMALE
    /*logIn = () => {
        var email = this.state.email;
        var password = this.state.password;
        firebaseAppInitialized.auth().signInWithEmailAndPassword(email, password)
            .then(async () => {
                this.refs.toast.show('Accesso effettuato!', 750);

                /// CONTROLLO SE VUOLE RIMANERE CONNESSO
                if (this.state.rimaniConnesso === true) {
                    await AsyncStorage.multiSet([['rimaniConnesso', 'true'], ['email', email], ['password', password], ['admin', 'false']], (error) => {

                        error === null ? console.log('Info salvate') : console.log(error);

                    })
                } else {
                    await AsyncStorage.multiSet([['rimaniConnesso', 'false'], ['email', ''], ['password', ''], ['admin', 'false']], (error) => {

                        error === null ? console.log('Non rimane connesso.') : console.log(error);

                    })
                }

                this.registerForPushNotificationsAsync(email);

                this.props.navigation.navigate('Account');
            })
            .catch((error) => {
                this.refs.toast.show('Errore, riprovare', 750);
                console.log(error);

            });
    }*/

    //LOGIN ADMIN
    logIn = async () => {
        console.log('Tento l\'accesso..');

        var email = this.state.email;
        var password = this.state.password;
        firebaseAppInitialized.auth().signInWithEmailAndPassword(email, password)
            .then(async () => {

                firebaseAppInitialized.auth().app.firestore().collection('Utenti')
                    .doc(email).get()
                    .then(async (doc) => {
                        if (doc.data().admin === true) {
                            console.log('L\' utente è un admin');
                            this.refs.toast.show('Accesso effettuato come ADMIN', 750);
                            // L'UTENTE E' UN ADMIN
                            // NAVIGA ALLA SEZIONE ADMIN
                            // CONTROLLO SE VUOLE RIMANERE CONNESSO
                            if (this.state.rimaniConnesso === true) {
                                await AsyncStorage.multiSet([['rimaniConnesso', 'true'], ['email', email], ['password', password], ['admin', 'true']], (error) => {

                                    error === null ? console.log('Info salvate') : console.log(error);

                                })
                            } else {
                                await AsyncStorage.multiSet([['rimaniConnesso', 'false'], ['email', ''], ['password', ''], ['admin', 'false']], (error) => {

                                    error === null ? console.log('Non rimane connesso.') : console.log(error);

                                })
                            }

                            await this.registerForPushNotificationsAsync(email);

                            this.props.navigation.navigate('AdminScreen');
                        } else {
                            // L'UTENTE NON E' UN ADMIN
                            this.refs.toast.show('Accesso effettuato!', 750);

                            /// CONTROLLO SE VUOLE RIMANERE CONNESSO
                            if (this.state.rimaniConnesso === true) {
                                await AsyncStorage.multiSet([['rimaniConnesso', 'true'], ['email', email], ['password', password], ['admin', 'false']], (error) => {

                                    error === null ? console.log('Info salvate') : console.log(error);

                                })
                            } else {
                                await AsyncStorage.multiSet([['rimaniConnesso', 'false'], ['email', ''], ['password', ''], ['admin', 'false']], (error) => {

                                    error === null ? console.log('Non rimane connesso.') : console.log(error);

                                })
                            }

                            await this.registerForPushNotificationsAsync(email);

                            this.props.navigation.navigate('Account');
                        }
                    })
            })
            .catch((error) => {
                this.refs.toast.show('Errore, riprovare', 750);
                console.log(error);

            });
    }

    registerForPushNotificationsAsync = async (email) => {
        var token = '';
        if (Constants.isDevice) {
            const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
            let finalStatus = existingStatus;
            if (existingStatus !== 'granted') {
                const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
                finalStatus = status;
            }
            if (finalStatus !== 'granted') {
                alert('Failed to get push token for push notification!');
                return;
            }
            console.log(finalStatus);
            token = await Notifications.getExpoPushTokenAsync();
            console.log(token);
            this.setState({ expoPushToken: token });

            firebaseAppInitialized.auth().app.firestore().collection('Utenti')
                .doc(email).update({
                    ExponentPushToken: token,
                })
        } else {
            alert('Must use physical device for Push Notifications');
        }

        if (Platform.OS === 'android') {
            Notifications.createChannelAndroidAsync('default', {
                name: 'default',
                sound: true,
                priority: 'max',
                vibrate: [0, 250, 250, 250],
            });
        }
    };

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            <View
                style={{
                    height: '90%',
                    paddingLeft: width * 0.03,
                    paddingRight: width * 0.03,
                    borderWidth: 0
                }}
            >

                <Text
                    style={{
                        fontSize: width * 0.1,
                        marginTop: height * 0.05,
                        fontWeight: 'bold',
                    }}
                >
                    {"Login"}
                </Text>

                <View
                    style={{
                        marginTop: height * 0.02,
                        borderBottomColor: '#BCBBC1',
                        borderBottomWidth: 1
                    }}
                />

                <Text
                    style={{
                        fontSize: width * 0.055,
                        marginTop: height * 0.04,
                    }}
                >
                    {"Per poter effettuare una prenotazione devi essere loggato, puoi farlo qui!"}
                </Text>
                <ScrollView
                    style={{
                        height: '70%',
                        paddingBottom: 100,
                    }}
                    containerStyle={{
                        paddingBottom: 100,
                    }}
                    horizontal={false}
                    scrollEnabled={true}
                    behaviour="height"
                    bounces={false}
                >
                    {/* INPUT EMAIL */}
                    <Input
                        containerStyle={{
                            marginTop: height * 0.075,
                        }}
                        placeholder='E-mail'
                        rightIcon={
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        email: "",
                                    })
                                }}
                            >
                                <MaterialIcons
                                    name='cancel'
                                    size={24}
                                    color='#808080'
                                />
                            </TouchableOpacity>
                        }
                        value={this.state.email}
                        onChangeText={(text) => {
                            this.setState({
                                email: text,
                            }, () => {
                                console.log(this.state.email);
                            });
                        }}
                        keyboardType='email-address'
                        inputStyle={{
                            fontSize: height * 0.03,
                        }}
                        autoCapitalize='none'
                    />

                    {/* INPUT PASSWORD */}
                    <Input
                        containerStyle={{
                            marginTop: height * 0.075,
                        }}
                        placeholder='Password'
                        rightIcon={
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        password: "",
                                    })
                                }}
                            >
                                <MaterialIcons
                                    name='cancel'
                                    size={24}
                                    color='#808080'
                                />
                            </TouchableOpacity>
                        }
                        secureTextEntry={true}
                        value={this.state.password}
                        onChangeText={(text) => {
                            this.setState({
                                password: text,
                            }, () => {
                                console.log(this.state.password);
                            });
                        }}
                        inputStyle={{
                            fontSize: height * 0.03,
                        }}
                    />

                    {/* CHECKBOX PER RIMANERE LOGGATI */}
                    <CheckBox
                        title='Resta connesso'
                        checked={this.state.rimaniConnesso}
                        containerStyle={{
                            marginTop: height * 0.02,
                            backgroundColor: 'white',
                            borderWidth: 0
                        }}
                        onPress={() => {
                            this.setState({
                                rimaniConnesso: !this.state.rimaniConnesso,
                            })
                        }}
                    />

                    <View
                        style={{
                            flexDirection: 'row',
                            width: '100%',
                            marginTop: height * 0.03,
                            paddingBottom: 5,
                        }}
                    >

                        {/* COLONNA - ACCEDI/REGISTRAZIONE */}
                        {/* COLONNA ACCEDI */}
                        <View
                            style={{
                                width: '50%',
                                borderWidth: 0,
                                marginLeft: 0,
                                marginRight: 'auto',
                            }}
                        >
                            <Button
                                buttonStyle={{
                                    backgroundColor: 'black',
                                    borderRadius: 100,
                                }}
                                containerStyle={{
                                    width: '70%',
                                    marginLeft: 'auto',
                                    marginRight: 'auto',
                                }}
                                title={"ACCEDI"}
                                titleStyle={{
                                    fontSize: height * 0.02,
                                    fontWeight: 'bold',
                                    color: 'white'
                                }}
                                onPress={() => {
                                    this.logIn();
                                }}
                            />
                        </View>
                        <View
                            style={{
                                width: 20,
                                marginLeft: 'auto',
                                marginRight: 'auto',
                                borderWidth: 0,
                            }}
                        >

                            <Text
                                style={{
                                    fontSize: height * 0.02,
                                    marginLeft: 'auto',
                                    marginRight: 'auto',
                                    width: '70%',
                                    borderWidth: 0,
                                    textAlign: 'center'
                                }}
                            >
                                {"o"}
                            </Text>
                        </View>
                        {/* COLONNA REGISTRATI */}
                        <View
                            style={{
                                width: '50%',
                                marginLeft: 'auto',
                                marginRight: 0,
                                borderWidth: 0,
                            }}
                        >
                            <Button
                                type='outline'
                                buttonStyle={{

                                    backgroundColor: 'white',
                                    borderRadius: 50,
                                    borderColor: 'black',
                                    borderWidth: 1,
                                }}
                                containerStyle={{
                                    width: '70%',
                                    marginLeft: 'auto',
                                    marginRight: 'auto',
                                    borderRadius: 50,
                                }}
                                title={"REGISTRATI"}
                                titleStyle={{
                                    fontSize: height * 0.0175,
                                    fontWeight: '100',
                                    color: 'black'
                                }}
                                onPress={() => {

                                    this.props.navigation.navigate('Registrazione');
                                }}
                            />
                        </View>
                    </View>
                    {/* BOTTONE LOGIN CON FACEBOOK 
                    <Button
                        icon={
                            <EvilIcons
                                name="sc-facebook"
                                size={30}
                                color="white"
                            />
                        }
                        iconLeft
                        title={"Login con Facebook"}
                        titleStyle={{
                            fontSize: width * 0.04,
                        }}
                        containerStyle={{
                            padding: 5,
                            marginTop: '10%',
                            marginBottom: '10%'
                        }}
                        buttonStyle={{
                            backgroundColor: '#3B5999',
                            height: height * 0.1
                        }}
                        onPress={() => {
                            this.logInWithFacebook();
                        }}
                    />*/}

                    {/* 
                    <TouchableOpacity
                        onPress={this.logInAdmin}
                    >
                        <View
                            style={{
                                width: '85%',
                                height: height * 0.06,
                                backgroundColor: 'black',
                                marginLeft: 'auto',
                                marginRight: 'auto',
                                marginTop: height * 0.04,
                                borderRadius: 100,
                                alignContent: 'center',
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}
                        >
                            <Text
                                style={{
                                    color: 'white',
                                    fontWeight: 'bold'
                                }}
                            >
                                {'ACCEDI COME ADMIN'}
                            </Text>
                        </View>
                    </TouchableOpacity>*/}
                </ScrollView >
                <Toast
                    ref="toast"
                    fadeOutDuration={250}
                    fadeInDuration={250}
                    textStyle={{
                        fontWeight: '100',
                        color: 'white'
                    }}
                    style={{
                        borderRadius: 100,
                        paddingLeft: width * 0.1,
                        paddingRight: width * 0.1,
                    }}
                    positionValue={height * 0.25}
                />
            </View >
        );
    }
}
