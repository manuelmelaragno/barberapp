import React from 'react';
import { AsyncStorage, Modal, BackHandler, Dimensions, StyleSheet, Text, View, StatusBar, Platform, SafeAreaView, Image, Linking, ScrollView, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';
import { Input, Button, CheckBox } from 'react-native-elements';
import { EvilIcons, AntDesign, SimpleLineIcons, MaterialIcons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../firebase/firebase';
import * as Facebook from 'expo-facebook';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast, { DURATION } from 'react-native-easy-toast';

export default class InfoAccount extends React.Component {

    constructor() {
        super();

        this.state = {
            nomeCognome: '',
            nTelefono: '+39',
            email: '',
            loadingSpinner: true,
            modalDatiMancantiVisibility: false,
        }

    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);

        this.getAccountInfo();

    }

    getAccountInfo = async () => {

        //CONTROLLARE SE L'ACCESSO E' TRAMITE FACEBOOK (CONTROLLARE CHE ESISTA IL TOKEN)
        //IN CASO USARE LE GRAPH API PER PRENDERE LE INFORMAZIONI
        //SE NON ESISTE IL NUMERO DI TELEFONO, CREARE UN MODAL PER FARLO INSERIRE E SALVARLO SUL DB
        var facebookToken = '';
        await AsyncStorage.getItem('facebookToken', (error, result) => {
            facebookToken = result;
            console.log("Token : " + result);
        });
        if (facebookToken !== null && facebookToken !== '') {

            console.log("Token : " + facebookToken);


            var response = await fetch('https://graph.facebook.com/me?fields=id,name,birthday,email&access_token=' + facebookToken);

            var utente = await response.json();

            console.log('Logged in!', utente);

            var nTelefono = '';
            await AsyncStorage.getItem('nTelefono', (error, result) => {
                nTelefono = result;
                console.log('nTelefono : ' + nTelefono);

            })

            this.setState({
                nomeCognome: utente.name,
                email: utente.email,
                nTelefono: nTelefono === null ? '+39' : nTelefono,
                modalDatiMancantiVisibility: nTelefono === null ? true : false,
                loadingSpinner: false,
            });

        } else {

            var user = firebaseAppInitialized.auth().currentUser;

            if (user) {
                var email = user.email;
                console.log(email);

                firebaseAppInitialized.auth().app.firestore().collection('Utenti').doc(email).get()
                    .then((doc) => {
                        console.log(doc.data());
                        this.setState({
                            nomeCognome: doc.data().nomeCognome,
                            nTelefono: doc.data().cellulare,
                            email: email,
                            loadingSpinner: false,
                        });
                    }).catch((error) => {
                        console.log(error);
                        this.setState({
                            loadingSpinner: false,
                        })
                    });
            }
        }
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        // works best when the goBack is async

        this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });

        return true;
    }

    logOut = async () => {

        await AsyncStorage.multiSet([['facebookToken', ''], ['rimaniConnesso', 'false'], ['email', ''], ['password', ''], ['admin', 'false']], (errors) => { })
            .then(() => {
                firebaseAppInitialized.auth().app.firestore().collection('Utenti')
                    .doc(this.state.email).update({
                        ExponentPushToken: '',
                    })
                firebaseAppInitialized.auth().signOut().then(() => {
                    this.props.navigation.navigate("Login");
                })
            })
    }

    render() {
        var { height, width } = Dimensions.get('window');
        //AsyncStorage.setItem('nTelefono', '+39');

        var nTelefonoRender = '+39';
        AsyncStorage.getItem('nTelefono', (error, result) => {
            nTelefonoRender = result;
            console.log('nTelefonoRender : ' + nTelefonoRender);

        })

        let nTelefonoMancanteRender = (
            <Input
                label={'Numero di telefono'}
                labelStyle={{
                    fontWeight: '100',
                    color: 'black'
                }
                }
                selectionColor='#007AFF'
                containerStyle={{
                    marginTop: height * 0.035,
                }}
                placeholder='Numero di telefono'
                rightIcon={
                    < TouchableOpacity
                        onPress={() => {
                            this.setState({
                                nTelefono: "",
                            })
                        }}
                    >
                        <MaterialIcons
                            name='cancel'
                            size={24}
                            color='#808080'
                        />
                    </TouchableOpacity >
                }
                value={this.state.nTelefono}
                onChangeText={(text) => {
                    this.setState({
                        nTelefono: text,
                    }, () => {
                        console.log('nTelefono : ' + this.state.nTelefono);
                    });
                }}
                keyboardType='phone-pad'
                inputStyle={{
                    fontSize: width * 0.045,
                }}
                autoCapitalize='none'
            />);

        return (
            <View
                style={{
                    height: "90%",
                }}
            >

                {/* MODAL INSERIMENTO DATI MANCANTI*/}
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalDatiMancantiVisibility}
                    onRequestClose={() => {

                    }}>
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: 'rgba(0,0,0,0.5)'
                        }}
                    >
                        <View
                            style={{
                                backgroundColor: 'white',
                                width: width * 0.95,
                                height: height * 0.7,
                                alignSelf: 'center',
                                marginTop: 'auto',
                                marginBottom: 'auto',
                                padding: '2%',
                                elevation: 10,

                                shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 5,
                                },
                                shadowOpacity: 0.34,
                                shadowRadius: 6.27,

                                padding: '5%',

                                borderRadius: 5
                            }}
                        >
                            <Text
                                style={{
                                    fontSize: width * 0.065,
                                    marginTop: height * 0.00,
                                    fontWeight: 'bold',
                                }}
                            >
                                {'Dati mancanti'}
                            </Text>
                            <Text
                                style={{
                                    fontSize: width * 0.04,
                                    marginTop: height * 0.01,
                                }}
                            >
                                {'Mancano dei dati, servono al barbiere per contattarti in caso di inconvenienze.'}
                            </Text>

                            {nTelefonoRender === '+39' ? nTelefonoMancanteRender : null}

                            <Button
                                buttonStyle={{
                                    backgroundColor: 'black',
                                    borderRadius: 100,
                                }}
                                containerStyle={{
                                    width: '30%',
                                    marginLeft: 'auto',
                                    marginRight: width * 0.03,
                                    marginTop: 'auto',
                                    marginBottom: 0,
                                }}
                                title={"SALVA"}
                                titleStyle={{
                                    fontSize: height * 0.015,
                                    fontWeight: 'bold',
                                    color: 'white'
                                }}
                                onPress={async () => {
                                    // CONTROLLO E SALVO IL NUMERO DI TELEFONO
                                    var nTelefono = this.state.nTelefono;
                                    let nTelefonoRegEx = /\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/;
                                    if (nTelefonoRegEx.test(nTelefono)) {
                                        // SALVO SU MEMORIA LOCALE
                                        await AsyncStorage.setItem('nTelefono', nTelefono);
                                        this.setState({
                                            modalDatiMancantiVisibility: false,
                                        })
                                    } else {
                                        this.refs.toast.show('Numero non valido!', 750);
                                    }
                                }}
                            />

                        </View>
                        <Toast
                            ref="toast"
                            fadeOutDuration={250}
                            fadeInDuration={250}
                            textStyle={{
                                fontWeight: '100',
                                color: 'white'
                            }}
                            style={{
                                borderRadius: 100,
                                paddingLeft: width * 0.1,
                                paddingRight: width * 0.1,
                            }}
                        />
                    </View>
                </Modal>
                {/* FINE MODAL */}
                <Spinner
                    visible={this.state.loadingSpinner}
                    textContent={'Carico informazioni account'}
                    textStyle={{
                        color: 'white'
                    }}
                    animation='fade'
                    overlayColor='rgba(0, 0, 0, 0.7)'
                />
                <ScrollView
                    style={{
                        height: "90%",
                        paddingBottom: 500,
                        paddingLeft: width * 0.03,
                        paddingRight: width * 0.03,
                    }}
                    containerStyle={{
                        flexGrow: 1,
                    }}
                    horizontal={false}
                    scrollEnabled={true}
                    behaviour="height"
                    bounces={false}
                >
                    <View
                        style={{
                            flexDirection: 'row'
                        }}
                    >
                        <View>
                            <Text
                                style={{
                                    fontSize: width * 0.1,
                                    marginTop: height * 0.05,
                                    fontWeight: 'bold',
                                }}
                            >
                                {"Account"}
                            </Text>
                        </View>

                        {/* BOTTONE LOGOUT */}
                        <View
                            style={{
                                marginRight: 0,
                                marginLeft: 'auto'
                            }}
                        >
                            <TouchableOpacity
                                style={{
                                    marginLeft: 'auto',
                                    marginRight: 0,
                                    marginTop: height * 0.05,
                                }}
                                onPress={() => {
                                    this.logOut();
                                }}
                            >
                                <AntDesign
                                    name={"logout"}
                                    color="#007AFF"
                                    size={30}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View
                        style={{
                            marginTop: height * 0.02,
                            borderBottomColor: '#BCBBC1',
                            borderBottomWidth: 1
                        }}
                    />
                    {/* AVATAR ACCOUNT */}
                    <View
                        style={{
                            width: width * 0.4,
                            height: width * 0.4,
                            backgroundColor: '#e6e6e6',
                            borderRadius: 100,
                            marginRight: 'auto',
                            marginLeft: 'auto',
                            marginTop: '15%',
                            justifyContent: 'center',
                            alignContent: 'center',
                            alignItems: 'center',
                            paddingTop: height * 0.03
                        }}
                    >
                        <Text
                            style={{
                                flex: 1,
                                fontSize: width * 0.2,
                                fontWeight: '200',
                                alignSelf: 'center',
                                textAlignVertical: 'center',
                            }}
                        >
                            {this.state.nomeCognome.charAt(0).toUpperCase()}
                        </Text>
                    </View>
                    {/* ACCOUNT INFO */}
                    <View
                        style={{
                            borderWidth: 0,
                            marginTop: '15%',
                        }}
                    >
                        <Text
                            style={{
                                fontSize: width * 0.05,
                            }}
                        >
                            {
                                'Nome : ' + this.state.nomeCognome + '\n\n' +
                                'Numero : ' + this.state.nTelefono + '\n\n' +
                                'E-mail : ' + this.state.email + '\n\n'
                            }
                        </Text>
                    </View>
                </ScrollView >
            </View >
        );
    }
}
