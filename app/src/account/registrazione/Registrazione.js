import React from 'react';
import { Alert, KeyboardAvoidingView, AsyncStorage, BackHandler, Dimensions, StyleSheet, Text, View, StatusBar, Platform, SafeAreaView, Image, Linking, ScrollView, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import Constants from 'expo-constants';
import { Input, Button, CheckBox } from 'react-native-elements';
import { EvilIcons, AntDesign, SimpleLineIcons, MaterialIcons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../firebase/firebase';
import * as Facebook from 'expo-facebook';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Toast, { DURATION } from 'react-native-easy-toast';

export default class InfoAccount extends React.Component {

    constructor() {
        super();

        this.state = {
            email: '',
            password: '',
            cellulare: '+39',
            nomeCogome: '',

        }
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        // works best when the goBack is async

        /*this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });*/
        Alert.alert(
            'Uscire dall\'app?',
            'Vuoi uscire dall\'app?',
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Sì', onPress: () => { console.log('OK Pressed'), BackHandler.exitApp(); } },
            ],
            { cancelable: false }
        );
        return true;
    }

    registrati = () => {

        var email = this.state.email;
        let emailRegEx = /(?:[a-z0-9!#$%&' * +/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
        var password = this.state.password;
        var cellulare = this.state.cellulare;
        let cellulareRegEx = /\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$/;
        var nomeCognome = this.state.nomeCogome;

        if (emailRegEx.test(email)) { //Vedo se la mail è valida
            if (password.length > 7) { //Vedo se la password è valida
                if (cellulareRegEx.test(cellulare)) {// Vedo se il numero è valido
                    //CREO UTENTE SALVO LE INFO SU FIREBASE
                    firebaseAppInitialized.auth().createUserWithEmailAndPassword(email, password)
                        .then(() => {
                            firebaseAppInitialized.firestore().collection('Utenti').doc(email).set({
                                nomeCognome: nomeCognome,
                                cellulare: cellulare,
                                email: email,
                                admin: false,
                            }).then(() => {
                                this.refs.toast.show('Registrazione effettuata!', 750);
                                firebaseAppInitialized.auth().signOut();
                                this.props.navigation.navigate('Login');
                            })
                        })
                        .catch((error) => {
                            console.log(error.code + " : " + error.message);
                            if (error.code === 'auth/email-already-in-use') {
                                this.refs.toast.show('Indirizzo e-mail già registrato', 750);
                            } else {
                                this.refs.toast.show('Errore, riprovare più tardi', 750);
                            }
                        });
                } else {
                    this.refs.toast.show('Numero non valido', 750);
                }
            } else {
                this.refs.toast.show('Password troppo corta', 750);
            }
        } else {
            this.refs.toast.show('Email non valida', 750);
            console.log('Email non valida');
        }
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (

            <View
                style={{
                    paddingLeft: width * 0.03,
                    paddingRight: width * 0.03,
                }}
            >
                <Text
                    style={{
                        fontSize: width * 0.1,
                        marginTop: height * 0.05,
                        fontWeight: 'bold',
                    }}
                >
                    {"Registrazione"}
                </Text>

                <View
                    style={{
                        marginTop: height * 0.02,
                        borderBottomColor: '#BCBBC1',
                        borderBottomWidth: 1
                    }}
                />
                <Text
                    style={{
                        fontSize: width * 0.055,
                        marginTop: height * 0.04,
                    }}
                >
                    {"Registrati qui!"}
                </Text>
                <KeyboardAwareScrollView
                    enableOnAndroid={true}
                    keyboardOpeningTime={0}
                    bounces={false}
                >

                    {/* INPUT NOME E COGNOME */}
                    <Input
                        selectionColor='#007AFF'
                        containerStyle={{
                            marginTop: height * 0.035,
                        }}
                        placeholder='Nome e Cognome'
                        rightIcon={
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        nomeCogome: "",
                                    })
                                }}
                            >
                                <MaterialIcons
                                    name='cancel'
                                    size={24}
                                    color='#808080'
                                />
                            </TouchableOpacity>
                        }
                        value={this.state.nomeCogome}
                        onChangeText={(text) => {
                            this.setState({
                                nomeCogome: text,
                            }, () => {
                                console.log('Nome e Cognome : ' + this.state.nomeCogome);
                            });
                        }}
                        keyboardType='default'
                        inputStyle={{
                            fontSize: width * 0.045,
                        }}
                    />

                    {/* INPUT NUMERO DI CELLULARE */}
                    <Input
                        selectionColor='#007AFF'
                        containerStyle={{
                            marginTop: height * 0.035,
                        }}
                        placeholder='Numero di telefono'
                        rightIcon={
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        cellulare: "",
                                    })
                                }}
                            >
                                <MaterialIcons
                                    name='cancel'
                                    size={24}
                                    color='#808080'
                                />
                            </TouchableOpacity>
                        }
                        value={this.state.cellulare}
                        onChangeText={(text) => {
                            this.setState({
                                cellulare: text,
                            }, () => {
                                console.log('Cellulare : ' + this.state.cellulare);
                            });
                        }}
                        keyboardType='phone-pad'
                        inputStyle={{
                            fontSize: width * 0.045,
                        }}
                        autoCapitalize='none'
                    />

                    {/* INPUT EMAIL */}
                    <Input
                        selectionColor='#007AFF'
                        containerStyle={{
                            marginTop: height * 0.035,
                        }}
                        placeholder='Indirizzo e-mail'
                        rightIcon={
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        email: "",
                                    })
                                }}
                            >
                                <MaterialIcons
                                    name='cancel'
                                    size={24}
                                    color='#808080'
                                />
                            </TouchableOpacity>
                        }
                        value={this.state.email}
                        onChangeText={(text) => {
                            this.setState({
                                email: text,
                            }, () => {
                                console.log('Email : ' + this.state.email);
                            });
                        }}
                        keyboardType='email-address'
                        inputStyle={{
                            fontSize: width * 0.045,
                        }}
                        autoCapitalize='none'
                    />

                    {/* INPUT PASSWORD */}
                    <Input
                        selectionColor='#007AFF'
                        containerStyle={{
                            marginTop: height * 0.035,
                        }}
                        placeholder='Password'
                        rightIcon={
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({
                                        password: "",
                                    })
                                }}
                            >
                                <MaterialIcons
                                    name='cancel'
                                    size={24}
                                    color='#808080'
                                />
                            </TouchableOpacity>
                        }
                        value={this.state.password}
                        onChangeText={(text) => {
                            this.setState({
                                password: text,
                            }, () => {
                                console.log('Password : ' + this.state.password);
                            });
                        }}
                        keyboardType='default'
                        inputStyle={{
                            fontSize: width * 0.045,
                        }}
                        secureTextEntry={true}
                    />

                    {/* BOTTONE REGISTRATI */}
                    <Button
                        buttonStyle={{
                            backgroundColor: 'black',
                            borderRadius: 100,
                        }}
                        containerStyle={{
                            width: '40%',
                            marginLeft: 'auto',
                            marginRight: width * 0.03,
                            marginTop: height * 0.03,
                        }}
                        title={"REGISTRATI"}
                        titleStyle={{
                            fontSize: height * 0.02,
                            fontWeight: 'bold',
                            color: 'white'
                        }}
                        onPress={() => {
                            this.registrati();
                        }}
                    />

                    <Text
                        style={{
                            fontSize: height * 0.02,
                            marginLeft: 'auto',
                            marginRight: width * 0.03,
                            marginTop: height * 0.02,
                            width: '40%',
                            borderWidth: 0,
                            textAlign: 'center'
                        }}
                    >
                        {"oppure"}
                    </Text>

                    {/* BOTTONE ACCEDI */}
                    <Button
                        type='outline'
                        buttonStyle={{

                            backgroundColor: 'white',
                            borderRadius: 50,
                            borderColor: 'black',
                            borderWidth: 1,
                        }}
                        containerStyle={{
                            width: '40%',
                            marginLeft: 'auto',
                            marginRight: width * 0.03,
                            marginTop: height * 0.02,
                            borderRadius: 50,
                        }}
                        title={"ACCEDI"}
                        titleStyle={{
                            fontSize: height * 0.0175,
                            fontWeight: '100',
                            color: 'black'
                        }}
                        onPress={() => {
                            this.props.navigation.navigate('Login');
                        }}
                    />
                </KeyboardAwareScrollView>
                <Toast
                    ref="toast"
                    fadeOutDuration={250}
                    fadeInDuration={250}
                    textStyle={{
                        fontWeight: '100',
                        color: 'white'
                    }}
                    style={{
                        borderRadius: 100,
                        paddingLeft: width * 0.1,
                        paddingRight: width * 0.1,
                    }}
                />
            </View>
        );
    }
}
