import React from 'react';
import { Dimensions, Modal, StyleSheet, ActivityIndicator, Text, View, StatusBar, Platform, AsyncStorage, SafeAreaView, Image, Linking, ScrollView, TouchableWithoutFeedback, BackHandler } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons } from '@expo/vector-icons';
import Toast, { DURATION } from 'react-native-easy-toast';

export default class LoadModal extends React.Component {

    constructor() {
        super();

        this.state = {

        };
    }

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <Modal
                visible={this.props.visible}
                transparent={true}
                style={{
                    height: "90%",
                }}
            >
                <View
                    style={{
                        flex: 1,
                        backgroundColor: 'rgba(0,0,0,0.5)',
                        alignContent: 'center',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <View
                        style={{
                            width: width * 0.5,
                            height: width * 0.5,
                            backgroundColor: 'white',
                            borderRadius: 25,
                            alignContent: 'center',
                            alignItems: 'center',
                            paddingTop: width * 0.1,
                        }}
                    >
                        <Text
                            style={{
                                paddingBottom: width * 0.1,
                            }}>
                            {this.props.titolo}
                        </Text>
                        <ActivityIndicator size={Platform.OS === 'ios' ? 'large' : width * 0.15} color="black" />
                    </View>
                </View>
            </Modal >
        );
    }
}
