import React from 'react';
import { Dimensions, StyleSheet, Text, View, StatusBar, Platform, AsyncStorage, SafeAreaView, Image, Linking, ScrollView, TouchableWithoutFeedback, BackHandler, FlatList, RefreshControl } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons } from '@expo/vector-icons';
import firebaseAppInitialized from '../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';

export default class Prodotti extends React.Component {

    constructor() {
        super();

        this.state = {
            prodotti: [],
            loadingProdotti: true,
            limitQuery: 15,
            pivot: null,
        };
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.caricaProdotti();
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        // works best when the goBack is async

        /*this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });*/
        Alert.alert(
            'Uscire dall\'app?',
            'Vuoi uscire dall\'app?',
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Sì', onPress: () => { console.log('OK Pressed'), BackHandler.exitApp(); } },
            ],
            { cancelable: false }
        );
        return true;
    }

    caricaProdotti = () => {
        console.log('Carico prodotti...');

        var pivot = this.state.pivot;
        var limit = this.state.limitQuery;
        var user = firebaseAppInitialized.auth().currentUser;

        firebaseAppInitialized.auth().app.firestore().collection('Prodotti')
            .orderBy('nome').startAfter(pivot).limit(limit)
            .get()
            .then((querySnapshot) => {
                if (querySnapshot.docs.length > 0) {
                    pivot = querySnapshot.docs[querySnapshot.docs.length - 1];
                    //console.log(pivot);
                    querySnapshot.forEach((doc) => {
                        // doc.data() is never undefined for query doc snapshots
                        console.log(doc.id, " => ", doc.data());

                        var prodotti = this.state.prodotti;
                        prodotti.push({
                            nome: doc.data().nome,
                            uriFoto: doc.data().uriFoto,
                            descrizione: doc.data().descrizione,
                            idProdotto: doc.id,
                        })
                        this.setState({
                            prodotti: prodotti,
                            loadingProdotti: false,
                        })
                    });

                    this.setState({
                        pivot: pivot,
                    })
                }
            })
    }

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <View
                style={{
                    height: "90%",
                }}
            >
                <Image
                    source={require('../../res/logo_orizzontale.png')}
                    style={{ width: width, height: height * 0.25 }}
                    resizeMode='contain'
                />

                {/* LISTA PRODOTTI */}
                <FlatList
                    data={this.state.prodotti}
                    renderItem={({ item }) =>
                        <Item nome={item.nome} uriFoto={item.uriFoto} descrizione={item.descrizione} idProdotto={item.idProdotto} eliminaProdotto={this.eliminaProdotto} />
                    }
                    keyExtractor={item => item.idProdotto}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.loadingProdotti}
                            onRefresh={() => {
                                this.setState({
                                    pivot: null,
                                    loadingProdotti: true,
                                    prodotti: [],
                                }, () => {
                                    this.caricaProdotti()
                                })
                            }}
                        />
                    }
                    numColumns={2}
                    onEndReachedThreshold={0.3}
                    onEndReached={() => {
                        console.log('Carico altri prodotti...');
                        this.caricaProdotti();
                    }}
                    columnWrapperStyle={{ justifyContent: "space-around" }}
                    contentContainerStyle={{ paddingBottom: 20, }}
                />
            </View >
        );
    }
}

function Item({ nome, uriFoto, descrizione, idProdotto, eliminaProdotto }) {
    var { height, width } = Dimensions.get('window');
    return (
        <View
            style={{
                borderWidth: 0,
                width: width * 0.45,
                height: height * 0.4,
                marginTop: height * 0.02,
                backgroundColor: '#f7f7f7',
                borderRadius: 5,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,

                elevation: 5,
            }}
        >
            <View
                style={{
                    borderWidth: 0,
                    borderColor: 'red',
                    width: '100%',
                    height: height * 0.2,
                }}
            >
                <Image
                    source={{ uri: uriFoto }}
                    style={{ width: '100%', height: '100%', borderTopRightRadius: 5, borderTopLeftRadius: 5, }}
                />
            </View>
            {/* COLONNA 2 - INFO PRODOTTO */}
            <View
                style={{
                    borderWidth: 0,
                    borderColor: 'red',
                    width: '100%',
                    height: height * 0.146,
                    paddingHorizontal: width * 0.02
                }}
            >
                <Text
                    style={{
                        fontSize: width * 0.0425,
                        fontWeight: 'bold',
                        marginTop: width * 0.0125,
                    }}
                >
                    {nome}
                </Text>
                <ScrollView
                    nestedScrollEnabled={true}
                    style={{
                        width: '100%',
                        height: '100%',
                        borderWidth: 0,
                    }}
                    showsVerticalScrollIndicator={false}
                >
                    <Text
                        style={{
                            fontSize: width * 0.0375,
                            color: '#222222',
                            marginTop: width * 0.0125,
                        }}
                    >
                        {descrizione}
                    </Text>
                </ScrollView>
            </View>
        </View>
    )
}

