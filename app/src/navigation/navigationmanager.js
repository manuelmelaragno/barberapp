import React, { Component } from 'react';
import { Animated, Easing } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Benvenuto from '../benvenuto/benvenuto'
import PrenotaTaglio from '../prenota taglio/prenotaTaglio'
import Login from '../account/login/Login'
import Registrazione from '../account/registrazione/Registrazione'
import InfoAccount from '../account/info/InfoAccount'
import Splash from '../splashscreen/splash'
import AdminScreen from '../account/admin/AdminScreen'
import AdminPrenotazioni from '../account/admin/prenotazioni/AdminPrenotazioni'
import AdminProdotti from '../account/admin/prodotti/AdminProdotti'
import Prodotti from '../prodotti/prodotti'
import AdminOrari from '../account/admin/orari/AdminOrari'

const NavigationStack = createStackNavigator(
    {
        /*Splash: {
            screen: Splash,
        },*/
        Benvenuto: {
            screen: Benvenuto,
        },
        PrenotaTaglio: {
            screen: PrenotaTaglio,
        },
        Login: {
            screen: Login,
        },
        Registrazione: {
            screen: Registrazione,
        },
        Account: {
            screen: InfoAccount
        },
        Prodotti: {
            screen: Prodotti
        },
        AdminScreen: {
            screen: AdminScreen
        },
        AdminPrenotazioni: {
            screen: AdminPrenotazioni
        },
        AdminProdotti: {
            screen: AdminProdotti
        },
        AdminOrari: {
            screen: AdminOrari
        },
    },
    {
        initialRouteKey: 'Benvenuto',
        defaultNavigationOptions: {
            //opzioni dell'header, in questo caso, nullo
            header: null,
            headerLeft: null,
            gesturesEnabled: false
        },
    },
);

const Container = createAppContainer(NavigationStack);

export default Container;