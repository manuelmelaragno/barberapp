import React from 'react';
import { Dimensions, StyleSheet, Text, View, StatusBar, ActivityIndicator, Platform, SafeAreaView, Image, Linking, ScrollView, TouchableWithoutFeedback, AsyncStorage } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons } from '@expo/vector-icons';
import firebaseAppInitialized from '../firebase/firebase';

export default class Splash extends React.Component {

    constructor() {
        super();
    }

    async componentDidMount() {
        // CONTROLLO CHE L'UTENTE ABBIA FATTO L'ACCESSO CON FACEBOOK O SIA RIMASTO CONNESSO
        var rimaniConnesso = '';
        var facebookToken = '';
        var user = firebaseAppInitialized.auth().currentUser;
        console.log('User : ' + user === null);


        await AsyncStorage.getItem('rimaniConnesso', (error, result) => {
            rimaniConnesso = result;
            console.log('Rimani connesso : ' + rimaniConnesso);
        })

        await AsyncStorage.getItem('facebookToken', (error, result) => {
            facebookToken = result;
            console.log('facebookToken : ' + facebookToken);
        })

        if (user || (rimaniConnesso !== null && rimaniConnesso !== 'false') || (facebookToken !== null && facebookToken !== '')) {
            //L'UTENTE HA EFFETTUATO L'ACCESSO
            if (user === null && (rimaniConnesso !== null && rimaniConnesso !== 'false')) {
                // ACCEDERE A FIREBASE, L'UTENTE HA IL 'rimaniConnesso'
                var email = '';
                var password = '';

                await AsyncStorage.getItem('email', (error, result) => {
                    email = result;
                    console.log('Email : ' + email);
                }).then(async () => {
                    await AsyncStorage.getItem('password', (error, result) => {
                        password = result;
                        console.log('Password : ' + password);
                    })
                })

                firebaseAppInitialized.auth().signInWithEmailAndPassword(email, password)
                    .then(() => {
                        console.log('Accesso effettuato');
                        this.props.navigation.navigate('Benvenuto');
                    })
                    .catch((error) => {
                        console.log('Errors : ' + error);
                    });
            } else if (user) {
                console.log('Accesso effettuato');
                this.props.navigation.navigate('Benvenuto');
            }

        } else {
            console.log('Accesso non effettuato');
            this.props.navigation.navigate('Benvenuto');
        }
    }

    render() {
        var { height, width } = Dimensions.get('window');
        return (
            <View
                style={{
                    flex: 1,
                    alignContent: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <Text>
                    {'Caricamento...\n\n\n\n'}
                </Text>
                <ActivityIndicator size={Platform.OS === 'ios' ? 'large' : width * 0.15} color="black" />
            </View >
        );
    }
}
