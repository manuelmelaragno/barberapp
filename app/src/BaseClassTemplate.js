import React from 'react';
import { Dimensions, StyleSheet, Text, View, StatusBar, Platform, AsyncStorage, SafeAreaView, Image, Linking, ScrollView, TouchableWithoutFeedback, BackHandler } from 'react-native';
import Constants from 'expo-constants';
import { EvilIcons, AntDesign, SimpleLineIcons } from '@expo/vector-icons';
import firebaseAppInitialized from '../../firebase/firebase';
import Toast, { DURATION } from 'react-native-easy-toast';

export default class Nome_Classe extends React.Component {

    constructor() {
        super();

        this.state = {

        };
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
    }

    componentWillUnmount() {
        this.backHandler.remove()
    }

    handleBackPress = () => {
        // works best when the goBack is async

        /*this.props.navigation.navigate({ routeName: 'Benvenuto', transitionStyle: 'horizontal' });*/
        Alert.alert(
            'Uscire dall\'app?',
            'Vuoi uscire dall\'app?',
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Sì', onPress: () => { console.log('OK Pressed'), BackHandler.exitApp(); } },
            ],
            { cancelable: false }
        );
        return true;
    }

    render() {
        var { height, width } = Dimensions.get('window');


        return (
            <View
                style={{
                    height: "90%",
                }}
            >

            </View >
        );
    }
}
