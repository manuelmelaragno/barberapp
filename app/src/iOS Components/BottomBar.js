import React from 'react';
import { Alert, AsyncStorage, StyleSheet, Text, View, StatusBar, Platform, Dimensions, Image, DrawerLayoutAndroid, TouchableWithoutFeedback, BackHandler, Keyboard } from 'react-native';
import { Appbar } from 'react-native-paper';
import { EvilIcons, AntDesign } from '@expo/vector-icons';
import firebaseAppInitialized from '../firebase/firebase';

export default class BottomBar extends React.Component {

    constructor() {
        super();

        this.state = {
            selected: '1',
            visible: true,
        }


    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
    }

    componentWillUnmount() {
        this.backHandler.remove();
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }

    _keyboardDidShow = () => {
        this.setState({
            visible: Platform.OS == 'android' ? false : true,
        })
    }

    _keyboardDidHide = () => {
        this.setState({
            visible: true,
        })
    }

    handleBackPress = () => {
        // works best when the goBack is async
        Alert.alert(
            'Uscire dall\'app?',
            'Vuoi uscire dall\'app?',
            [
                {
                    text: 'No',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'Sì', onPress: () => { console.log('OK Pressed'), BackHandler.exitApp(); } },
            ],
            { cancelable: false }
        );
        return true;
    }

    render() {
        var { height, width } = Dimensions.get('window');

        return (
            this.state.visible &&
            <View style={[{ position: 'absolute', flexDirection: 'row', height: this.state.visible === true ? height * 0.1 : 0, width: width, bottom: 0, }, styles.shadowBar]}>
                <TouchableWithoutFeedback onPress={() => { //TASTO HOME
                    console.log("Item 1 premuto");
                    this.setState({
                        selected: '1'
                    })
                    this.props.navigateTo('Benvenuto');
                }}>
                    <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>

                        <AntDesign name='home'
                            size={height * 0.039}
                            color={this.state.selected === '1' ? '#007AFF' : 'black'}
                        />
                        <Text style={{ fontWeight: '100', fontSize: height * 0.02, color: this.state.selected === '1' ? '#007AFF' : 'black' }}>Home</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => { //TASTO PRENOTA TAGLIO
                    console.log("Item 2 premuto");
                    this.props.navigateTo('PrenotaTaglio');
                    this.setState({
                        selected: '2'
                    });
                }}>
                    <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>

                        <EvilIcons name='calendar'
                            size={height * 0.05}
                            color={this.state.selected === '2' ? '#007AFF' : 'black'}
                        />
                        <Text style={{ fontWeight: '100', fontSize: height * 0.02, color: this.state.selected === '2' ? '#007AFF' : 'black' }}>Taglio</Text>
                    </View>
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback onPress={() => { //TASTO PRODOTTI
                    console.log("Item 3 premuto");
                    this.setState({
                        selected: '3'
                    }, () => {
                        this.props.navigateTo('Prodotti')
                    })
                }}>
                    <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>

                        <EvilIcons name='cart'
                            size={height * 0.05}
                            color={this.state.selected === '3' ? '#007AFF' : 'black'}
                        />
                        <Text style={{ fontWeight: '100', fontSize: height * 0.02, color: this.state.selected === '3' ? '#007AFF' : 'black' }}>Prodotti</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={async () => { //TASTO ACCOUNT - SE L'UTENTE HA SALVATO I PROPRI DATI ALLORA VADO DIRETTAMENTE ALL'ACCOUNT
                    var user = firebaseAppInitialized.auth().currentUser;
                    var rimaniConnesso = '';
                    var facebookToken = '';
                    var admin = '';

                    await AsyncStorage.getItem('rimaniConnesso', (error, result) => {
                        rimaniConnesso = result;
                        console.log('Rimani connesso : ' + rimaniConnesso);
                    })

                    // VERIFICO CHE L'UTENTE SIA UN ADMIN
                    await AsyncStorage.getItem('admin', (error, result) => {
                        admin = result;
                        console.log('Admin : ' + rimaniConnesso);
                    })

                    await AsyncStorage.getItem('facebookToken', (error, result) => {
                        facebookToken = result;
                        console.log('facebookToken : ' + facebookToken);
                    });

                    user === null ? console.log('User non loggato') : console.log('User loggato');


                    if (user !== null || (facebookToken !== null && facebookToken !== '') || (rimaniConnesso !== null && rimaniConnesso !== 'false')) {
                        admin !== '' && admin !== 'false' ? this.props.navigateTo('AdminScreen') : this.props.navigateTo('Account');
                    } else {
                        this.props.navigateTo('Login');
                    }
                    this.setState({
                        selected: '4'
                    })
                }}>
                    <View style={{ flex: 1, alignContent: 'center', alignItems: 'center', justifyContent: 'center' }}>

                        <EvilIcons name='user'
                            size={height * 0.05}
                            color={this.state.selected === '4' ? '#007AFF' : 'black'}
                        />
                        <Text style={{ fontWeight: '100', fontSize: height * 0.02, color: this.state.selected === '4' ? '#007AFF' : 'black' }}>Account</Text>
                    </View>
                </TouchableWithoutFeedback>
            </View >
        );
    }
}
const styles = StyleSheet.create({
    shadowBar: {
        backgroundColor: '#f7f7f7',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }
});