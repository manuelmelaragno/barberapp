import React from 'react';
import { StyleSheet, Text, View, StatusBar, Platform, SafeAreaView, Dimensions } from 'react-native';
import Contanier from './app/src/navigation/navigationmanager'
import Constants from 'expo-constants';
import BottomBar from './app/src/iOS Components/BottomBar'
import { YellowBox } from 'react-native';
import _ from 'lodash';
import { decode, encode } from 'base-64';

global.crypto = require("firebase/firestore");
global.crypto.getRandomValues = byteArray => { for (let i = 0; i < byteArray.length; i++) { byteArray[i] = Math.floor(256 * Math.random()); } }
if (!global.btoa) { global.btoa = encode; }
if (!global.atob) { global.atob = decode; }


export default class App extends React.Component {

  constructor() {
    super();

    console.ignoredYellowBox = [
      'Setting a timer'
    ];

    YellowBox.ignoreWarnings(['Setting a timer']);
    const _console = _.clone(console);
    console.warn = message => {
      if (message.indexOf('Setting a timer') <= -1) {
        _console.warn(message);
      }
    };

    this.state = {
      bottomBarAbilitata: false,
      screen: 'Splash',
      fontLoaded: false,
    }
  }

  componentDidMount() {

  }

  navigateTo = (title) => {
    this.Container._navigation.navigate(title);
    //console.log(this.Container);
  }

  getCurrentScreen = () => {
    return this.Container._navigation.state.index.toString();
  }

  render() {

    let color = Platform.OS === 'android' ? 'black' : 'black';
    var { height, width } = Dimensions.get('window');

    return (
      <View style={{ flex: 1, backgroundColor: color, paddingTop: Platform.OS === 'android' ? 0 : Constants.statusBarHeight }}>
        <StatusBar barStyle="light-content" />
        <Contanier ref={ref => this.Container = ref} />
        <BottomBar navigateTo={this.navigateTo} />
      </View>
    );
  }
}
